SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `Sistema_01` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `Sistema_01`;

-- -----------------------------------------------------
-- Table `Sistema_01`.`persona`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Sistema_01`.`persona` (
  `idpersona` INT NOT NULL ,
  `per_nombre` VARCHAR(100) NOT NULL ,
  `per_apel_pat` VARCHAR(100) NOT NULL ,
  `per_apel_mat` VARCHAR(100) NOT NULL ,
  `per_nombre_completo` VARCHAR(150) NOT NULL ,
  `per_dni` INT NOT NULL ,
  `per_genero` TINYINT NOT NULL ,
  PRIMARY KEY (`idpersona`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sistema_01`.`usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Sistema_01`.`usuario` (
  `idusuario` INT NOT NULL ,
  `usu_nickname` VARCHAR(45) NOT NULL ,
  `usu_password` VARCHAR(45) NOT NULL ,
  `persona_idpersona` INT NULL ,
  PRIMARY KEY (`idusuario`) ,
  INDEX `fk_usuario_persona` (`persona_idpersona` ASC) ,
  CONSTRAINT `fk_usuario_persona`
    FOREIGN KEY (`persona_idpersona` )
    REFERENCES `Sistema_01`.`persona` (`idpersona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sistema_01`.`rol`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Sistema_01`.`rol` (
  `idrol` INT NOT NULL AUTO_INCREMENT ,
  `rol_nombre` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idrol`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sistema_01`.`rol-usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Sistema_01`.`rol-usuario` (
  `rol_idrol` INT NOT NULL ,
  `usuario_idusuario` INT NOT NULL ,
  PRIMARY KEY (`rol_idrol`, `usuario_idusuario`) ,
  INDEX `fk_rol_has_usuario_rol` (`rol_idrol` ASC) ,
  INDEX `fk_rol_has_usuario_usuario` (`usuario_idusuario` ASC) ,
  CONSTRAINT `fk_rol_has_usuario_rol`
    FOREIGN KEY (`rol_idrol` )
    REFERENCES `Sistema_01`.`rol` (`idrol` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rol_has_usuario_usuario`
    FOREIGN KEY (`usuario_idusuario` )
    REFERENCES `Sistema_01`.`usuario` (`idusuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
