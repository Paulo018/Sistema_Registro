package AccesoDB;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConectaDB 
{
 public static Connection ObtenerConexion() throws Exception// la excepcion lo opera quien llama al metodo
 {//Cualquier manejador de base de datos se puede abrir
     Connection cn =null;
        //donde esta tus base de datos y donde esta tu driver para conectarte
     String url = "jdbc:mysql://localhost:3306/sistema_01";
        //si existe otro manejador de base de datos cambiariamos la url
     Class.forName("com.mysql.jdbc.Driver");//driver de conexion para el manejador de base de datos este driver cambia segun la base de datos, el proecto deberia  de tener el driver
     cn= DriverManager.getConnection(url,"root","1234");
     return cn;
 }
 public static void Cerrarconexion(Connection cn) throws Exception
 { 
     if(cn.isClosed() == false)// si cn no esta cerrado entonces se cierra
     {
         cn.close();
     }
 }
}
