package AccesoDB;
import java.sql.CallableStatement;
import java.sql.Connection;
    
public class OperaDB 
{
    private static Connection cn;
    public static CallableStatement getCst(String Sql)throws  Exception //Esto es para llamr cualquier metodo del a base mysql
    {
        cn=ConectaDB.ObtenerConexion();
        if(cn == null)
        {
            throw new Exception("No hay conexión");// con el throw la excepcion la manipula quien llame al método
        }
        else
        {
            return cn.prepareCall(Sql);//Prepara la llamada de los metodos
        }
    }
    public static void Ejecutar(CallableStatement Cst) throws  Exception
    {
        try 
        {
            Cst.execute();
        } catch (Exception e) 
        {
            throw new Exception("No se pudo ejecutar instrucción ");
        }
    }
    public static void Cerrar() throws Exception
    {
      ConectaDB.Cerrarconexion(cn);
    }
            
    
}
