package Componentes;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

public class Button_gen extends JButton implements FocusListener, MouseListener
{
    private Dimension Dimension = new Dimension(210 ,40);
    
    public Color ColorFondoDefault = new Color(0,73,83);
    public Color ColorFondoAction = new Color(12, 85, 100);
    public Color ColorTextoDefault = new Color(255, 255, 255);
    public Color ColorTextoAction = new Color(235, 235, 235);
    
    public Font Fuente = new Font("Open Sans", Font.BOLD, 16 );

    public Button_gen()
    {
        Button_gen.this.setSize(Dimension);
        Button_gen.this.setPreferredSize(Dimension);
        Button_gen.this.setVisible(true);
        Button_gen.this.setBorder(null);
        Button_gen.this.setBorderPainted(false);
        Button_gen.this.setContentAreaFilled(false);
        Button_gen.this.setBackground(ColorFondoDefault);
        Button_gen.this.setForeground(ColorTextoDefault);
        Button_gen.this.setFont(Fuente);
        Button_gen.this.setCursor(new Cursor(Cursor.HAND_CURSOR));
        
        Button_gen.this.setOpaque(true);
        Button_gen.this.setFocusPainted(false);
        Button_gen.this.addFocusListener(Button_gen.this);
        Button_gen.this.addMouseListener(Button_gen.this);
    }
    
    public void ButtonAction()
    {
        Button_gen.this.setBackground(ColorFondoAction);
        Button_gen.this.setForeground(ColorTextoAction);
    }
    public void ButtonDefault()
    {
        Button_gen.this.setBackground(ColorFondoDefault);
        Button_gen.this.setForeground(ColorTextoDefault);
    }

    /*EVENTOS DE MOUSE*/
    
    @Override
    public void focusGained(FocusEvent fe) {
        ButtonAction();
        setPressedIcon(getPressedIcon());
        setRolloverIcon(getRolloverIcon());
    }

    @Override
    public void focusLost(FocusEvent fe) {
        ButtonDefault();
    }

    @Override
    public void mouseClicked(MouseEvent me)
    {
    }

    @Override
    public void mousePressed(MouseEvent me) {
        ButtonAction();
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        ButtonDefault();
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        ButtonAction();
    }

    @Override
    public void mouseExited(MouseEvent me) {
        if(me.getClickCount() == 1)
        {
            ButtonAction();
        }
        else
        {
            ButtonDefault();
        }
    }
    
    //GETTER Y SETTER

    public Color getColorFondoDefault() {
        return ColorFondoDefault;
    }

    public void setColorFondoDefault(Color ColorFondoDefault) {
        this.ColorFondoDefault = ColorFondoDefault;
    }

    public Color getColorFondoAction() {
        return ColorFondoAction;
    }

    public void setColorFondoAction(Color ColorFondoAction) {
        this.ColorFondoAction = ColorFondoAction;
    }

    public Color getColorTextoDefault() {
        return ColorTextoDefault;
    }

    public void setColorTextoDefault(Color ColorTextoDefault) {
        this.ColorTextoDefault = ColorTextoDefault;
    }

    public Color getColorTextoAction() {
        return ColorTextoAction;
    }

    public void setColorTextoAction(Color ColorTextoAction) {
        this.ColorTextoAction = ColorTextoAction;
    }
}
