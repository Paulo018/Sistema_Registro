package Componentes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;

public class JCheckBoxCustomIcon extends JCheckBox
{
    private final ImageIcon defecto = new ImageIcon(getClass().getResource("/Imagenes/checkbox-02.png"));
    private final ImageIcon selected = new ImageIcon(getClass().getResource("/Imagenes/checkbox-04.png"));
    private final ImageIcon rollovericon = new ImageIcon(getClass().getResource("/Imagenes/check-03.png"));
    private final ImageIcon rolloverselected = new ImageIcon(getClass().getResource("/Imagenes/check-04.png"));
    public int index;
    public Color ColorFondo = new Color(4,68,67);
    public Color ColorFondoDisabled = new Color(12, 85, 100);
    public Color ColorBorde = new Color(0, 30, 30);
    public Color ColorTexto = new Color(255, 255, 255);
    ArrayList Lista;
    
    public JCheckBoxCustomIcon() 
    {
        super();
    
        setFont(new Font("Fuentes/OpenSans-Regular_0.ttf", Font.PLAIN, 13 ));
        setForeground(ColorTexto);
        setOpaque(false);
        setIcon((defecto));
        setRolloverIcon(rollovericon);
        setSelectedIcon(selected);
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
//        setDisabledIcon(new ImageIcon("Imagenes/check-04.png"));
//        setDisabledSelectedIcon(new ImageIcon("Imagenes/check-03.png"));
//        setPressedIcon(new ImageIcon("Imagenes/check-01.png"));
        setRolloverSelectedIcon(rolloverselected);
        Lista = new ArrayList();

    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Color getColorFondo() {
        return ColorFondo;
    }

    public void setColorFondo(Color ColorFondo) {
        this.ColorFondo = ColorFondo;
    }

    public Color getColorFondoDisabled() {
        return ColorFondoDisabled;
    }

    public void setColorFondoDisabled(Color ColorFondoDisabled) {
        this.ColorFondoDisabled = ColorFondoDisabled;
    }

    public Color getColorBorde() {
        return ColorBorde;
    }

    public void setColorBorde(Color ColorBorde) {
        this.ColorBorde = ColorBorde;
    }

    public Color getColorTexto() {
        return ColorTexto;
    }

    public void setColorTexto(Color ColorTexto) {
        this.ColorTexto = ColorTexto;
    }
    
    
}
