package Componentes;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

public class Tabla extends JTable {
 
    private Color ColorFondo = new Color(255,255,255);
    private Color ColorSelectedFondo = new Color(0,45,55);
    private Color ColorTextRow = new Color(255, 255, 255);
    private Color ColorBorderGrid = new Color(12, 85, 100);

    public Tabla()
    {
        
        super();
        this.setAutoCreateColumnsFromModel(true);
        this.setAutoscrolls(true);
        this.setVisible(true);
        
        /** propiedades para el header */
        JTableHeader jtableHeader = this.getTableHeader();
        jtableHeader.setDefaultRenderer(new Tabla_Header());
        jtableHeader.setReorderingAllowed(false);
        
        this.setTableHeader(  jtableHeader );
        
        /** propiedades para las celdas */
        this.setSelectionBackground(ColorSelectedFondo);
        this.setSelectionForeground(ColorTextRow);
        this.setGridColor(ColorBorderGrid);
        this.setDefaultRenderer (Object.class, new Tabla_CellRenderer());
        this.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        this.setVisible(true);
        this.setRowHeight(20);
        this.setBackground(ColorFondo);
        this.setOpaque(true);
        
      
        
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    }

    
    @Override
    public boolean isCellEditable (int row, int column)// si queremos deshabilitar el llenado de las celdas
    {
        return false;
    }
    public Color getColorFondo() {
        return ColorFondo;
    }

    public void setColorFondo(Color ColorFondo) {
        this.ColorFondo = ColorFondo;
    }

    public Color getColorSelectedFondo() {
        return ColorSelectedFondo;
    }

    public void setColorSelectedFondo(Color ColorSelectedFondo) {
        this.ColorSelectedFondo = ColorSelectedFondo;
    }

    public Color getColorTextRow() {
        return ColorTextRow;
    }

    public void setColorTextRow(Color ColorTextRow) {
        this.ColorTextRow = ColorTextRow;
    }

    public Color getColorBorderGrid() {
        return ColorBorderGrid;
    }

    public void setColorBorderGrid(Color ColorBorderGrid) {
        this.ColorBorderGrid = ColorBorderGrid;
    }
    
}