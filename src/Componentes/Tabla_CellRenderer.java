package Componentes;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;

public class Tabla_CellRenderer  extends DefaultTableCellRenderer {
    
    private final Font Fuente = new Font("/Fuentes/OpenSans-Regular_0", Font.PLAIN, 14 );
    
    private Color ColorTextRow = new Color(240, 240, 240);
    private Color ColorRowPar = new Color(12, 85, 100);
    private Color ColorRowImpar = new Color(0,73,83);
    
    @Override
    public Component getTableCellRendererComponent ( JTable table, Object value, boolean selected, boolean focused, int row, int column )
    {    
        setEnabled(table == null || table.isEnabled());
        setBackground(ColorRowPar);
        setForeground(ColorTextRow);
        table.setFont(Fuente);
        
        
        
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        
        setBackground( (row % 2 == 1)?this.ColorRowPar:this.ColorRowImpar  );
        
        setBorder(new MatteBorder(10, 100, 10, 10, ColorTextRow));
        
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        
        return this;
    }

    public Color getColorTextRow() {
        return ColorTextRow;
    }

    public void setColorTextRow(Color ColorTextRow) {
        this.ColorTextRow = ColorTextRow;
    }

    public Color getColorRowPar() {
        return ColorRowPar;
    }

    public void setColorRowPar(Color ColorRowPar) {
        this.ColorRowPar = ColorRowPar;
    }

    public Color getColorRowImpar() {
        return ColorRowImpar;
    }

    public void setColorRowImpar(Color ColorRowImpar) {
        this.ColorRowImpar = ColorRowImpar;
    }
}
