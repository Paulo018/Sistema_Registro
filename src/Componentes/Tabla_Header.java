package Componentes;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

public class Tabla_Header implements TableCellRenderer
{
    private Font Fuente = new Font("/Fuentes/OpenSans-Regular_0", Font.BOLD, 15 );
    
    private Color ColorFondoHeader = new Color(0 , 63,73);
    private Color ColorTextHeader = new Color(255,255,255);
    private Color ColorBordeHeader = new Color(0, 50, 50);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
        JComponent jcomponent = null;
        
        if(value instanceof String ) {
            jcomponent = new JLabel((String) "   " + value);
            ((JLabel)jcomponent).setHorizontalAlignment(SwingConstants.LEFT );
            ((JLabel)jcomponent).setSize( 25, jcomponent.getWidth() );
            ((JLabel)jcomponent).setPreferredSize( new Dimension(6, jcomponent.getWidth()) );
        } 
        
        jcomponent.setEnabled(true);        
        jcomponent.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 1, ColorBordeHeader));
        jcomponent.setOpaque(true);
        jcomponent.setBackground( ColorFondoHeader );
        jcomponent.setForeground(ColorTextHeader);
        jcomponent.setFont(Fuente);
        jcomponent.setToolTipText("Campo #"+(column+1));
       
        return jcomponent;
    }

    public Color getColorFondoHeader() {
        return ColorFondoHeader;
    }

    public void setColorFondoHeader(Color ColorFondoHeader) {
        this.ColorFondoHeader = ColorFondoHeader;
    }

    public Color getColorTextHeader() {
        return ColorTextHeader;
    }

    public void setColorTextHeader(Color ColorTextHeader) {
        this.ColorTextHeader = ColorTextHeader;
    }

    public Color getColorBordeHeader() {
        return ColorBordeHeader;
    }

    public void setColorBordeHeader(Color ColorBordeHeader) {
        this.ColorBordeHeader = ColorBordeHeader;
    }
    
    
}
