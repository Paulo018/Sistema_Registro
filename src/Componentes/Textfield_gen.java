package Componentes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JTextField;

public class Textfield_gen extends JTextField
{
    
//    private boolean HayQueMostrar = true;
    private String Placeholder = "Ingrese algo.";
    private Dimension Dimension = new Dimension(200,30);
    private Color ColorPlaceholder= new Color(60, 140, 145);
    Font Fuente = new Font("Open sans", Font.PLAIN, 15 );
    
    public Color ColorFondo = new Color(0,73,83);
    public Color ColorFondoDisabled = new Color(12, 85, 100);
    public Color ColorBorde = new Color(0, 30, 30);
    public Color ColorTexto = new Color(255, 255, 255);

    public Textfield_gen()
    {
        super();
        setSize(Dimension);
        setPreferredSize(Dimension);
        setMinimumSize(Dimension);
        setVisible(true);
        setMargin( new Insets(0,6,0,0));
        setFont(Fuente);
        
        setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(ColorBorde),// prámetro del color del borde
                        BorderFactory.createEmptyBorder(0, 6, 0, 5) // parámetro del espaciado entre el borde y el texto
                 ));
        
        setBackground(ColorFondo);

        setForeground(ColorTexto);
        setCaretColor(ColorTexto);//color del puntero |
        setSelectionColor(ColorBorde);//color de selección del texto
        setSelectedTextColor(ColorTexto);//color del texto seleccionado
        
        if(this.isEnabled()==false)
        {
            setBackground(ColorFondoDisabled);
        }
    }

    
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if ( getText().length()==0 ) {
        
        g.setColor(ColorPlaceholder);
        g.setFont(new Font("Fuentes/OpenSans-Regular_0.ttf", Font.PLAIN, 13 ));
        
        g.drawString( Placeholder,
                     getMargin().left + 6,
                     (getSize().height)/2 + getFont().getSize()/2 - 3 );
      }}
    
    
    
    
    
    
    
    
    
    
    
    public String getPlaceholder() {
        return Placeholder;
    }

    public void setPlaceholder(String Placeholder) {
        this.Placeholder = Placeholder;
    }

    public Dimension getDimension() {
        return Dimension;
    }

    public void setDimension(Dimension Dimension) {
        this.Dimension = Dimension;
    }

    public Color getColorPlaceholder() {
        return ColorPlaceholder;
    }

    public void setColorPlaceholder(Color ColorPlaceholder) {
        this.ColorPlaceholder = ColorPlaceholder;
    }

    public Color getColorFondo() {
        return ColorFondo;
    }

    public void setColorFondo(Color ColorFondo) {
        this.ColorFondo = ColorFondo;
    }

    public Color getColorBorde() {
        return ColorBorde;
    }

    public void setColorBorde(Color ColorBorde) {
        this.ColorBorde = ColorBorde;
    }

    public Color getColorTexto() {
        return ColorTexto;
    }

    public void setColorTexto(Color ColorTexto) {
        this.ColorTexto = ColorTexto;
    }
}
