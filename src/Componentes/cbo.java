package Componentes;
import com.sun.glass.ui.Cursor;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class cbo extends JComboBox
{
    public Font Fuente = new Font("/Fuentes/OpenSans-Regular_0.ttf", Font.PLAIN, 15 );

    private Color ColorTextCbo = new Color(255, 255, 255);
    private Color Colorfondo = new Color(0,73,83);
    private Color ColorBorde = new Color(0,30,30);
 
    public cbo()
    {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(cbo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(cbo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(cbo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(cbo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Dimension dimension = new Dimension(200,30);
        setPreferredSize(dimension);
        setSize(dimension);      
        setForeground(ColorTextCbo);  
        setFont(Fuente);
        
        setBorder(BorderFactory.createCompoundBorder(
                            BorderFactory.createLineBorder(ColorBorde),// prámetro del color del borde
                        BorderFactory.createEmptyBorder(0,5,0,0) // parámetro del espaciado entre el borde y el texto
                 ));
        setUI(cbo_CustomUI.createUI(this));                
        setVisible(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setBackground(Colorfondo);
        setMaximumRowCount(5);
        
    }
    

    public Color getColorTextCbo() {
        return ColorTextCbo;
    }

    public void setColorTextCbo(Color ColorTextCbo) {
        this.ColorTextCbo = ColorTextCbo;
    }
}