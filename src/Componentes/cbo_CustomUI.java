package Componentes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class cbo_CustomUI extends BasicComboBoxUI
{
    private final ImageIcon Flecha = new ImageIcon(getClass().getResource("/Imagenes/Flecha-01.png"));
    private final ImageIcon Espacio = new ImageIcon(getClass().getResource("/Imagenes/Espacio.png"));
    
    private Color ColorFondoCbo = new Color(0,73,83);
    private Color ColorTextCbo = new Color(255, 255, 255);
    private Color ColorFondoLst = new Color(0,73,83);
    private Color ColorTextLst = new Color(255, 255, 255);
    private Color ColorSelectLst = new Color(12, 85, 100);
    
    public Font Fuente = new Font("/Fuentes/OpenSans-Regular_0.ttf", Font.PLAIN, 14 );

    public static ComboBoxUI createUI(JComponent c) {
        return new cbo_CustomUI();
    }
    
    @Override
    protected JButton createArrowButton() {
        JButton button = new JButton();
        
        button.setText("");
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setBackground(ColorFondoCbo);
        button.setIcon(Flecha);
        button.setOpaque(true);
        return button;
    }

    @Override
    public void paintCurrentValueBackground(Graphics g,
            Rectangle bounds,
            boolean hasFocus) {
        g.setColor(ColorFondoCbo);
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    //Pinta los items
    
    @Override
    protected ListCellRenderer createRenderer() {
        return new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                    boolean isSelected, boolean cellHasFocus) {

                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                list.setSelectionBackground(ColorFondoCbo);
                list.setSelectionForeground(ColorTextCbo);
                list.setFont(Fuente);
                list.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                setAutoscrolls(false);
                
                if (isSelected)
                {
                    setBackground(ColorSelectLst);
                    setForeground(ColorTextLst);
                }
                else
                {
                    setBackground(ColorFondoLst);
                    setForeground(ColorTextLst);
                }
                if (index != -1)
                {
                    setIcon(Espacio);
                }
                return this;
            }
        };
    }

    public Color getColorFondoCbo() {
        return ColorFondoCbo;
    }

    public void setColorFondoCbo(Color ColorFondoCbo) {
        this.ColorFondoCbo = ColorFondoCbo;
    }

    public Color getColorTextCbo() {
        return ColorTextCbo;
    }

    public void setColorTextCbo(Color ColorTextCbo) {
        this.ColorTextCbo = ColorTextCbo;
    }

    public Color getColorFondoLst() {
        return ColorFondoLst;
    }

    public void setColorFondoLst(Color ColorFondoLst) {
        this.ColorFondoLst = ColorFondoLst;
    }

    public Color getColorTextLst() {
        return ColorTextLst;
    }

    public void setColorTextLst(Color ColorTextLst) {
        this.ColorTextLst = ColorTextLst;
    }

    public Color getColorSelectLst() {
        return ColorSelectLst;
    }

    public void setColorSelectLst(Color ColorSelectLst) {
        this.ColorSelectLst = ColorSelectLst;
    }
    
    
}
