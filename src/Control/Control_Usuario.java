package Control;

import ControlDAO.UsuarioDAO;
import Modelo.Usuario;
import Vista.JFrame_Incio_sesion;
import Vista.JFrame_Principal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class Control_Usuario 
{
    public static ArrayList<Usuario> Lista = new ArrayList<>();
//    
    public static void Agregar(Usuario e)
    {
        Lista.add(e);
    }
    
    public static void Listar(DefaultTableModel Modtabla)
    {
        for (int i = 0; i < Lista.size(); i++) 
        {
            Modtabla.addRow(Lista.get(i).getRegistro());
            
        }
    }
    
    public void Cargar_de_bd()
    {
        try {
            for (int i = 0; i < UsuarioDAO.Consultar_todos().size(); i++) 
            {
                Agregar(UsuarioDAO.Consultar_todos().get(i));                
            }
        } catch (Exception ex) {
            Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
    
    

    public static ArrayList<Usuario> getLista() {
        return Lista;
    }

    public static void setLista(ArrayList<Usuario> Lista) {
        Control_Usuario.Lista = Lista;
    }
    
    
    
    
    
    
    
}
