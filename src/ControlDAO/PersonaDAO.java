package ControlDAO;

import AccesoDB.OperaDB;
import Modelo.Persona;
import Vista.JFrame_Principal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/* Ingeniería de Sistemas */
public class PersonaDAO 
{

    public static void Insertar(Persona Elemento) throws Exception 
     {
        
            CallableStatement Cst = OperaDB.getCst("{CALL sp_persona_insertar (?,?,?,?,?,?,?)}");
            Cst.setInt(1, Elemento.getId());
            Cst.setString(2, Elemento.getNombre());
            Cst.setString(3,Elemento.getApel_Paterno());
            Cst.setString(4,Elemento.getApel_Materno());
            Cst.setString(5,Elemento.getNombre_Completo());
            Cst.setInt(6,Elemento.getDNI());
            Cst.setInt(7,Elemento.getGenero());
            OperaDB.Ejecutar(Cst);
            OperaDB.Cerrar();
        
    }
//     public static void Insertar(ArrayList<Persona> Elemento) throws Exception 
//     {
//        for (int i = 0; i < Elemento.size(); i++) 
//        { 
//            CallableStatement Cst = OperaDB.getCst("{CALL sp_persona_insertar(?,?,?,?,?,?,?)}");
//            
//            Cst.setInt(1,Elemento.get(i).getId());
//            Cst.setString(2, Elemento.get(i).getNombre());
//            Cst.setString(3,Elemento.get(i).getApel_Paterno());
//            Cst.setString(4,Elemento.get(i).getApel_Materno());
//            Cst.setString(5,Elemento.get(i).getNombre_Completo());
//            Cst.setInt(6,Elemento.get(i).getDNI());
//            Cst.setInt(7,Elemento.get(i).getGenero());
//            OperaDB.Ejecutar(Cst);
//            OperaDB.Cerrar();
//        }
//    }
//     
//     public static void Insertar_Enlace(int e1, int e2, double e3) throws Exception {
//        CallableStatement Cst = OperaDB.getCst("{CALL sp_enlace_insertar (?,?,?)}");
//        Cst.setDouble(1, e3);
//        Cst.setInt(2,e1);
//        Cst.setInt(3, e2);
//        OperaDB.Ejecutar(Cst);
//        OperaDB.Cerrar();
//    }
     
//     public static void Insertar_Enlace(Ciudad Elemento) throws Exception {
//        CallableStatement Cst = OperaDB.getCst("{CALL sp_ciudad_insertar(?,?)}");
//        ResultSet rs = cst.getResultSet();
//        ArbolB Lista = new ArbolB();
//        while (rs.next())
//                {
//                    Producto Elemento  = new Producto();
//                    Elemento.setIdcodigo(rs.getInt(1));
//                    Elemento.setDescripcion(rs.getString(2));
//                    Elemento.setPeso(rs.getInt(3));
////                    Lista.setRaiz(Lista.Agregar(Lista.getRaiz(), Elemento));
//                    Lista.Agregar2(Elemento);
//                }
//        Cst.setString(1,Elemento.getNombre());
//        Cst.setInt(2, Elemento.getPoblacion());
//        OperaDB.Ejecutar(Cst);
//        OperaDB.Cerrar();
//    }
//    
//     
//     private static String Consultar_nombre(int id) throws Exception
//    {
//        CallableStatement cst = OperaDB.getCst("{ CALL sp_usuario_consultar_nombre (?,?)}");
//        cst.setInt(1,id);
//        cst.registerOutParameter(2,java.sql.Types.VARCHAR);
//        
//        OperaDB.Ejecutar(cst);
//        OperaDB.Cerrar(); 
//        return cst.getString(2);
//    }
//    }   
// 

    public static void Eliminar(Persona Elemento) throws Exception
    {
        CallableStatement cst = OperaDB.getCst("{ call sp_persona_eliminar (?)}");
        cst.setInt(1, Elemento.getId());
        OperaDB.Ejecutar(cst);
        OperaDB.Cerrar();   
    }
     
    public static ArrayList<Persona> Consultar_todos() throws Exception            
    {
        CallableStatement cst = OperaDB.getCst("{ call sp_persona_consultar_todo()}");
        OperaDB.Ejecutar(cst);
        ResultSet rs = cst.getResultSet();
        
        ArrayList<Persona> Lista = new ArrayList();
        
        Persona objPersona;
        while (rs.next())
                {
                    objPersona = new Persona(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4),
                                            rs.getString(5), rs.getInt(6), rs.getInt(7));
                    Lista.add(objPersona);
                }
        
        OperaDB.Cerrar(); 
        return Lista;
    }
    
    public static void Borrar_bd() throws Exception            
    {
        CallableStatement cst = OperaDB.getCst("{ call sp_persona_borrar_bd()}");
        OperaDB.Ejecutar(cst);
        
        OperaDB.Cerrar();
    }
    
    

//    public static Grafo Consultar_todos_enlaces() throws Exception            
//    {
//        JFrame_Principal p = new JFrame_Principal();
//        String a, b;
//        CallableStatement cst = OperaDB.getCst("{ call sp_enlace_consultar_todos ()}");
//        OperaDB.Ejecutar(cst);
//        ResultSet rs = cst.getResultSet();
//        Grafo Lista = new Grafo();
//        while (rs.next())
//                {
//                    int Origen =(rs.getInt(3));
//                    int Destino =(rs.getInt(4));
//                    double Distancia =(rs.getDouble(2));
//                    
//                    
//                    CallableStatement cst1 = OperaDB.getCst("{ CALL sp_ciudad_consultar_uno (?,?)}");
//                    cst1.setInt(1,Origen);
//                    cst1.registerOutParameter(2,java.sql.Types.VARCHAR);
//                    OperaDB.Ejecutar(cst1);
//                    a=cst1.getString(2);
//                    
//                    
//                    CallableStatement cst2 = OperaDB.getCst("{ CALL sp_ciudad_consultar_uno (?,?)}");
//                    cst2.setInt(1,Destino);
//                    cst2.registerOutParameter(2,java.sql.Types.VARCHAR);
//                    OperaDB.Ejecutar(cst2);
//                    
//                    b=cst2.getString(2);
//                    
//                    Nodo CiudadConsulta = p.objGrafo.BuscarCiudad(a);
//            
//                    OperaDB.Ejecutar(cst);
//                    Lista.Agregar_Enlace(a, b, Distancia);
//                    p.Llenar_Lista(a, CiudadConsulta.getAristas());
//                }
//        OperaDB.Cerrar(); 
//        return Lista;
//    }
       public static void Actualizar(Persona Elemento) throws Exception
    {
        CallableStatement cst = OperaDB.getCst("{ call sp_persona_actualizar (?,?,?,?,?,?,?)}");
        cst.setInt(1, Elemento.getId());
        cst.setString(2, Elemento.getNombre());
        cst.setString(3, Elemento.getApel_Paterno());
        cst.setString(4, Elemento.getApel_Materno());
        cst.setString(5, Elemento.getNombre_Completo());
        cst.setInt(6, Elemento.getDNI());
        cst.setInt(7, Elemento.getGenero());
        OperaDB.Ejecutar(cst);
        OperaDB.Cerrar();
        
    }
}
