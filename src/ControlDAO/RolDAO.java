package ControlDAO;

import AccesoDB.OperaDB;
import Modelo.Persona;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class RolDAO
{
    
    public static ArrayList<String> Consultar_todos() throws Exception            
    {
        CallableStatement cst = OperaDB.getCst("{ call sp_rol_consultar_todo()}");
        OperaDB.Ejecutar(cst);
        ResultSet rs = cst.getResultSet();
        
        ArrayList<String> Lista = new ArrayList();
        
        while (rs.next())
                {
                    Lista.add(rs.getString(2));
                }
        
        OperaDB.Cerrar(); 
        return Lista;
    }

    
}
