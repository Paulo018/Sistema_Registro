package ControlDAO;

import AccesoDB.OperaDB;
import Modelo.Rol_Usuario;
import Modelo.Usuario;
import java.sql.CallableStatement;

public class Rol_UsuarioDao
{
    public static void Insertar(Rol_Usuario Elemento) throws Exception 
     {
            CallableStatement Cst = OperaDB.getCst("{CALL sp_rol_usuario_insertar(?,?)}");
            Cst.setInt(1, Elemento.getId_Usuario());
            Cst.setInt(2,Elemento.getId_Rol());
            OperaDB.Ejecutar(Cst);
            OperaDB.Cerrar();
    }
    
    public static void Eliminar(Rol_Usuario Elemento) throws Exception
    {
        CallableStatement Cst = OperaDB.getCst("{CALL sp_rol_usuario_eliminar(?,?)}");
        Cst.setInt(1, Elemento.getId_Usuario());
        Cst.setInt(2, Elemento.getId_Rol());
        OperaDB.Ejecutar(Cst);
        OperaDB.Cerrar();
    }
    
}
