package ControlDAO;

import AccesoDB.OperaDB;
import Modelo.Usuario;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/* Ingeniería de Sistemas */
public class UsuarioDAO 
{

     public static void Insertar(Usuario Elemento) throws Exception 
     {
            CallableStatement Cst = OperaDB.getCst("{CALL sp_usuario_insertar(?,?,?)}");
            Cst.setString(1, Elemento.getNickname());
            Cst.setString(2,Elemento.getPassword());
            Cst.setInt(3,Elemento.getId_Persona());
            OperaDB.Ejecutar(Cst);
            OperaDB.Cerrar();
    }
//     
//     public static void Insertar_Enlace(int e1, int e2, double e3) throws Exception {
//        CallableStatement Cst = OperaDB.getCst("{CALL sp_enlace_insertar (?,?,?)}");
//        Cst.setDouble(1, e3);
//        Cst.setInt(2,e1);
//        Cst.setInt(3, e2);
//        OperaDB.Ejecutar(Cst);
//        OperaDB.Cerrar();
//    }
     
//     public static void Insertar_Enlace(Ciudad Elemento) throws Exception {
//        CallableStatement Cst = OperaDB.getCst("{CALL sp_ciudad_insertar(?,?)}");
//        ResultSet rs = cst.getResultSet();
//        ArbolB Lista = new ArbolB();
//        while (rs.next())
//                {
//                    Producto Elemento  = new Producto();
//                    Elemento.setIdcodigo(rs.getInt(1));
//                    Elemento.setDescripcion(rs.getString(2));
//                    Elemento.setPeso(rs.getInt(3));
////                    Lista.setRaiz(Lista.Agregar(Lista.getRaiz(), Elemento));
//                    Lista.Agregar2(Elemento);
//                }
//        Cst.setString(1,Elemento.getNombre());
//        Cst.setInt(2, Elemento.getPoblacion());
//        OperaDB.Ejecutar(Cst);
//        OperaDB.Cerrar();
//    }
//    
     
//     private static String Consultar_uno(int id) throws Exception
//    {
//        CallableStatement cst = OperaDB.getCst("{ CALL sp_ciudad_consultar_uno (?,?)}");
//        cst.setInt(1,id);
//        cst.registerOutParameter(2,java.sql.Types.VARCHAR);
//        
//        OperaDB.Ejecutar(cst);
//        OperaDB.Cerrar(); 
//        return cst.getString(2);
//    }
//    }   
// 
//      public static void Eliminar(Producto Elemento) throws Exception
//    {
//        CallableStatement cst = OperaDB.getCst("{ call sp_producto_eliminar (?)}");
//        cst.setInt(1, Elemento.getIdcodigo());
//        OperaDB.Ejecutar(cst);
//        OperaDB.Cerrar();   
//    }
//     
    public static ArrayList<Usuario> Consultar_todos() throws Exception            
    {
        CallableStatement cst = OperaDB.getCst("{ call sp_usuario_consultar_todo()}");
         OperaDB.Ejecutar(cst);
        ResultSet rs = cst.getResultSet();
        
        ArrayList<Usuario>  Lista = new ArrayList();
        
        while (rs.next())
                {
                    Usuario Elemento  = new Usuario();
                    Elemento.setId(rs.getInt(1));
                    Elemento.setId_Persona(rs.getInt(4));
                    Elemento.setNickname(rs.getString(2));
                    Elemento.setPassword(rs.getString(3));
                    Elemento.setNombre_Completo(rs.getString(5));
                    Elemento.setDNI(rs.getInt(6));
                    Lista.add(Elemento);
                    
                }
        OperaDB.Cerrar(); 
        return Lista;
    }
//    
//    
//    
//    public static ArrayList Consultar_todos_Categoria() throws Exception            
//    {
//        CallableStatement cst = OperaDB.getCst("{ call sp_categoria_consultar_todos()}");
//        OperaDB.Ejecutar(cst);
//        ResultSet rs = cst.getResultSet();
//        
//        ArrayList Lista = new ArrayList();
//        ArrayList Lista_1 = new ArrayList();
//        
//        while (rs.next())
//                {
//                    Lista.add(rs.getString(2));
//                }
//        
//        OperaDB.Cerrar(); 
//        return Lista;
//    }
//    public static Grafo Consultar_todos_enlaces() throws Exception            
//    {
//        JFrame_Principal p = new JFrame_Principal();
//        String a, b;
//        CallableStatement cst = OperaDB.getCst("{ call sp_enlace_consultar_todos ()}");
//        OperaDB.Ejecutar(cst);
//        ResultSet rs = cst.getResultSet();
//        Grafo Lista = new Grafo();
//        while (rs.next())
//                {
//                    int Origen =(rs.getInt(3));
//                    int Destino =(rs.getInt(4));
//                    double Distancia =(rs.getDouble(2));
//                    
//                    
//                    CallableStatement cst1 = OperaDB.getCst("{ CALL sp_ciudad_consultar_uno (?,?)}");
//                    cst1.setInt(1,Origen);
//                    cst1.registerOutParameter(2,java.sql.Types.VARCHAR);
//                    OperaDB.Ejecutar(cst1);
//                    a=cst1.getString(2);
//                    
//                    
//                    CallableStatement cst2 = OperaDB.getCst("{ CALL sp_ciudad_consultar_uno (?,?)}");
//                    cst2.setInt(1,Destino);
//                    cst2.registerOutParameter(2,java.sql.Types.VARCHAR);
//                    OperaDB.Ejecutar(cst2);
//                    
//                    b=cst2.getString(2);
//                    
//                    Nodo CiudadConsulta = p.objGrafo.BuscarCiudad(a);
//            
//                    OperaDB.Ejecutar(cst);
//                    Lista.Agregar_Enlace(a, b, Distancia);
//                    p.Llenar_Lista(a, CiudadConsulta.getAristas());
//                }
//        OperaDB.Cerrar(); 
//        return Lista;
    }
//       public static void Actualizar(Producto Elemento) throws Exception
//    {
//        CallableStatement cst = OperaDB.getCst("{ call sp_producto_actualizar (?,?,?)}");
//        cst.setInt(1, Elemento.getIdcodigo());
//        cst.setString(2,Elemento.getDescripcion());
//        cst.setInt(3,Elemento.getPeso());
//        OperaDB.Ejecutar(cst);
//        OperaDB.Cerrar();
//        
//    }
