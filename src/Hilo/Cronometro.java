package Hilo;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Cronometro extends Thread
{
    private  JLabel Cont;
    private boolean Estado= true;
    JDialog ObjDialog;
    public Cronometro(JLabel Cont) {
        this.Cont = Cont;
    }
    
    
    
    public void Detener()
    {
        Estado = false;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean Estado) {
        this.Estado = Estado;
    }
     
    
    
    @Override
    public void run()
    {
        long seg=6;
        while(Estado)
        {
            
            seg--;
            Cont.setText(seg+"");
            if(seg== 0)
            {
                Detener();
            }
            try 
            {
                sleep(1000);
            }
            catch (Exception e)
            {
            }
        }
    }

}
