package Hilo;

import Jdialogs.Notificacion;
import java.awt.Color;
import static java.lang.Thread.sleep;
import java.util.TimerTask;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class traslacion extends Thread
{
    private java.util.Timer tiempo;
    private TimerTask task;
    private boolean Estado= true;
    JPanel pnl;
    int x_ini; 
    int y_ini;
    int x_fin;
    int y_fin;
    int frame;
    int velocidad;
    float Opac;
    float refer;
    
    private  JLabel JD;

    public traslacion(JPanel pnl,int x_fin, int velocidad) 
    {
        this.pnl = pnl;
        this.x_fin = x_fin;
        this.velocidad = velocidad;
        frame=pnl.getX();
    }

    public traslacion(int velocidad) {
        this.velocidad = velocidad;
    }

    public traslacion() {
    }

    public traslacion(JLabel JD, float Opac) {
        this.JD = JD;
        this.Opac = Opac;
    }

    

    
    public void Detener()
    {
        Estado = false;
    }
    
    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean Estado) {
        this.Estado = Estado;
    }
    
    
    
    
    

    
    
    
    
    public boolean startAnimation_Horizontal_izq()throws Exception
    {
        tiempo = new java.util.Timer();
        task = new TimerTask()
                
        {
        @Override
            public void run()
            {
                frame--;
                if(frame>=x_fin)
                {
                    pnl.setLocation(frame, pnl.getY());
                    
                }
                else{frame=pnl.getX();}
                if(frame==x_fin)
                {
                    tiempo.cancel();//valida que no se ejecute infinitamente
                    task.cancel();
                }
            }
        };
        //se inicia la animacion                                            
        tiempo.schedule(task,0, velocidad);
        return true;
    }
    
    
    
    public boolean startAnimation_Horizontal_der()throws Exception
    {
        tiempo = new java.util.Timer();
        task = new TimerTask()
                
        {
        @Override
            public void run()
            {
                frame++;
                if(frame<=x_fin)
                {
                    pnl.setLocation(frame, pnl.getY());
                }
                else{frame=pnl.getX();}
                if(frame==x_fin)
                {
                    tiempo.cancel();//valida que no se ejecute infinitamente
                    task.cancel();
                }
            }
        };
        //se inicia la animacion                                            
        tiempo.schedule(task,0,velocidad);
        return true;
    }
    
    
    public boolean startAnimation_Opacidad()
    {
        tiempo = new java.util.Timer();
        task = new TimerTask()
        {
        @Override
            public void run()
            {
                refer++;
                if(refer <=Opac*10)
                {
                    JD.setBackground(new Color(0, 0, 0, refer/25.5F));
                }
                
                if(refer == Opac)
                {
                    tiempo.cancel();//valida que no se ejecute infinitamente
                    task.cancel();
                }
            }
        };
        //se inicia la animacion                                            
        tiempo.schedule(task,0,40);
        return true;
    }    
}

