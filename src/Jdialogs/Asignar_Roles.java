package Jdialogs;

import Componentes.JCheckBoxCustomIcon;
import Componentes.Textfield_gen;
import ControlDAO.PersonaDAO;
import ControlDAO.RolDAO;
import ControlDAO.Rol_UsuarioDao;
import Modelo.Persona;
import Modelo.Rol_Usuario;
import Modelo.Usuario;
import Vista.JFrame_Incio_sesion;
import Vista.JFrame_Principal;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JLayeredPane;

public class Asignar_Roles extends javax.swing.JDialog {
    
    RolDAO obj_RolDAO;
    JCheckBoxCustomIcon check;
    public static ArrayList<JCheckBoxCustomIcon> Lista_roles;

    public Asignar_Roles(java.awt.Frame parent, boolean modal)
    {
        super(parent, modal);
        setUndecorated(true);
        initComponents();
        Lista_roles = new ArrayList();
        txt_Nombre.requestFocus(); 
        obj_RolDAO = new RolDAO();
    }
    
    public  void Cargar_persona()
    {
        try
        {
        int Id = Integer.parseInt(txt_Id.getText()+"");
            for (int i = 0; i < Lista_roles.size(); i++)
            {
                if( Lista_roles.get(i).isSelected())
                {
                    Rol_UsuarioDao.Insertar(new Rol_Usuario(Id, i+1));
                }
                else
                {
                    Rol_UsuarioDao.Eliminar(new Rol_Usuario(Id, i+1));
                }
            }
        }
        catch(Exception e){}
        dispose();
    }

    public void crear_Checkbox()
    {
        int posy =2;
        try
        {
            Lista_roles.clear();
            for (int i = 0; i < obj_RolDAO.Consultar_todos().size(); i++)
            { 
                Lista_roles.add(new JCheckBoxCustomIcon());
                Lista_roles.get(i).setLocation(this.getX()+100, getY()+posy);
                jLayeredPane1.add(Lista_roles.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0+posy, -1, -1));
                Lista_roles.get(i).setText(obj_RolDAO.Consultar_todos().get(i));
                Lista_roles.get(i).setVisible(true);
                posy=posy+30;
            }
        } catch (Exception ex) { }
    }
    
    public void Modificar()
    {
//        Cargar_persona();
        
//        try 
//        {
//            for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
//            {
//                if(JFrame_Principal.Lista.get(i).getId()==Integer.parseInt(txt_Id.getText()))
//                {
//                    JFrame_Principal.Lista.get(i).setNombre_Completo(objUsuario.getNombre_Completo());
//                    JFrame_Principal.Agr_Persona.Modtabla.setRowCount(0);//en ves de limpiar y poner otra vez la lista en la tabla, lo que voya a hacer despues es que me recorra la tabla al igual que el arraylist de arriba y luego me reemplaze esa persona, pero primero analizar, ya que uno me a reemplazar todos los datos, pero la otra forma me va a recorrer todos los datos, alguno debe ser mejor, despues veo...
//                    JFrame_Principal.Agr_Persona.Llenar_Tabla();
//                    PersonaDAO.Actualizar(objUsuario);
//                    
//                }
//            }   
//        } catch (Exception e) {
//        }
        JFrame_Principal.Fondo.dispose();
        dispose();
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        txt_Id = new Componentes.Textfield_gen();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_Nombre = new Componentes.Textfield_gen();
        jLabel8 = new javax.swing.JLabel();
        button_gen1 = new Componentes.Button_gen();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jLayeredPane1 = new javax.swing.JLayeredPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(4, 50, 57));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_Id.setEditable(false);
        txt_Id.setPlaceholder("ID");
        jPanel1.add(txt_Id, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 30, 80, -1));

        jLabel2.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("Asignar Roles");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 240, 40));

        jLabel7.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("ID:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 30, 60, 30));

        txt_Nombre.setEditable(false);
        txt_Nombre.setPlaceholder("Ingrese nombre.");
        txt_Nombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txt_NombreMouseEntered(evt);
            }
        });
        txt_Nombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_NombreKeyPressed(evt);
            }
        });
        jPanel1.add(txt_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, -1, -1));

        jLabel8.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Usuario:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 120, 30));

        button_gen1.setText("Listo");
        button_gen1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen1ActionPerformed(evt);
            }
        });
        jPanel1.add(button_gen1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, 140, -1));

        jLabel6.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Roles:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 130, 80, 30));

        jScrollPane1.setBackground(new java.awt.Color(4, 50, 57));
        jScrollPane1.setBorder(null);

        jLayeredPane1.setBackground(new java.awt.Color(4, 50, 57));
        jLayeredPane1.setOpaque(true);
        jLayeredPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jScrollPane1.setViewportView(jLayeredPane1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, 220, 170));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void button_gen1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen1ActionPerformed
        Cargar_persona();
        JFrame_Principal.Fondo.dispose();
    }//GEN-LAST:event_button_gen1ActionPerformed

    private void txt_NombreMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_NombreMouseEntered
        
    }//GEN-LAST:event_txt_NombreMouseEntered

    private void txt_NombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_NombreKeyPressed
        if(evt.getKeyChar()==KeyEvent.VK_ENTER)
        {
            Modificar();
        }
    }//GEN-LAST:event_txt_NombreKeyPressed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        
        JFrame_Incio_sesion.jf.Salir();
        JFrame_Incio_sesion.jf.dispose();
    }//GEN-LAST:event_formWindowClosing


    public Textfield_gen getTxt_Id() {
        return txt_Id;
    }

    public void setTxt_Id(Textfield_gen txt_Id) {
        this.txt_Id = txt_Id;
    }

    public Textfield_gen getTxt_Nombre() {
        return txt_Nombre;
    }

    public void setTxt_Nombre(Textfield_gen txt_Nombre) {
        this.txt_Nombre = txt_Nombre;
    }

    public JLayeredPane getjLayeredPane1() {
        return jLayeredPane1;
    }

    public void setjLayeredPane1(JLayeredPane jLayeredPane1) {
        this.jLayeredPane1 = jLayeredPane1;
    }

    
    

    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Asignar_Roles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Asignar_Roles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Asignar_Roles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Asignar_Roles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Asignar_Roles dialog = new Asignar_Roles(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private Componentes.Button_gen button_gen1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private Componentes.Textfield_gen txt_Id;
    private Componentes.Textfield_gen txt_Nombre;
    // End of variables declaration//GEN-END:variables
}
