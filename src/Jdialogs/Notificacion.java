package Jdialogs;

import com.sun.awt.AWTUtilities;

public class Notificacion extends java.awt.Dialog {

    public Notificacion(java.awt.Frame parent, boolean modal) 
    {
        super(parent, modal);
        setUndecorated(true);
        initComponents();
        AWTUtilities.setWindowOpaque(this, false);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_2 = new javax.swing.JLabel();
        lbl_1 = new javax.swing.JLabel();
        btn_Ok = new Componentes.Button_gen();
        lbl_3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setUndecorated(true);
        setOpacity(0.0F);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_2.setBackground(new java.awt.Color(0, 0, 0));
        lbl_2.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        lbl_2.setForeground(new java.awt.Color(51, 51, 51));
        lbl_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_2.setText("29 de julio de 2016");
        lbl_2.setToolTipText("");
        lbl_2.setVerifyInputWhenFocusTarget(false);
        add(lbl_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 120, -1));

        lbl_1.setBackground(new java.awt.Color(0, 0, 0));
        lbl_1.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        lbl_1.setForeground(new java.awt.Color(51, 51, 51));
        lbl_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_1.setText("Paulo Enrique Vigo Mamani");
        lbl_1.setToolTipText("");
        lbl_1.setVerifyInputWhenFocusTarget(false);
        add(lbl_1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 240, -1));

        btn_Ok.setText("OK");
        btn_Ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_OkActionPerformed(evt);
            }
        });
        add(btn_Ok, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 80, 30));

        lbl_3.setBackground(new java.awt.Color(0, 0, 0));
        lbl_3.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        lbl_3.setForeground(new java.awt.Color(51, 51, 51));
        lbl_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_3.setText("Fecha de Creación:");
        lbl_3.setToolTipText("");
        lbl_3.setVerifyInputWhenFocusTarget(false);
        add(lbl_3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 120, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/jdialog_notif_1.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 260, 160));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        System.exit(0);
    }//GEN-LAST:event_closeDialog

    private void btn_OkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_OkActionPerformed
        dispose();
    }//GEN-LAST:event_btn_OkActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Notificacion dialog = new Notificacion(new java.awt.Frame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Componentes.Button_gen btn_Ok;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lbl_1;
    private javax.swing.JLabel lbl_2;
    private javax.swing.JLabel lbl_3;
    // End of variables declaration//GEN-END:variables
}
