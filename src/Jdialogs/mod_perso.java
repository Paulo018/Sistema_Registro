package Jdialogs;

import Componentes.Textfield_gen;
import Componentes.cbo;
import ControlDAO.PersonaDAO;
import Modelo.Persona;
import Validaciones.Val_Keytiped;
import Vista.JFrame_Incio_sesion;
import Vista.JFrame_Principal;
import java.awt.event.KeyEvent;

public class mod_perso extends javax.swing.JDialog {
            Persona objPersona;

    public mod_perso(java.awt.Frame parent, boolean modal)
    {
        super(parent, modal);
        setUndecorated(true);
        initComponents();
        txt_Nombre.requestFocus(); 
    }
    
    public  Persona Cargar_persona()
    {
        int Id = Integer.parseInt(txt_Id.getText()+"");
        String Nombre = txt_Nombre.getText();
        String Apel_Pat = txt_Apel_Paterno.getText();
        String Apel_Mat = txt_Apel_Materno.getText();
        String Slug = txt_Nombre.getText()+ " " +txt_Apel_Paterno.getText()+ " " +txt_Apel_Materno.getText();
        int DNI = Integer.parseInt(txt_DNI.getText()+"");
        int Genero = cbo_Genero.getSelectedIndex();
        return objPersona = new Persona(Id, Nombre, Apel_Pat, Apel_Mat, Slug, DNI, Genero);
    }

    
    public void Modificar()
    {
        Cargar_persona();
        
        try 
        {
            for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
            {
                if(JFrame_Principal.Lista.get(i).getId()==Integer.parseInt(txt_Id.getText()))
                {
                    JFrame_Principal.Lista.get(i).setNombre(objPersona.getNombre());
                    JFrame_Principal.Lista.get(i).setApel_Paterno(objPersona.getApel_Paterno());
                    JFrame_Principal.Lista.get(i).setApel_Materno(objPersona.getApel_Materno());
                    JFrame_Principal.Lista.get(i).setDNI(objPersona.getDNI());
                    JFrame_Principal.Lista.get(i).setGenero(objPersona.getGenero());
                    JFrame_Principal.Lista.get(i).setNombre_Completo(objPersona.getNombre_Completo());
                    JFrame_Principal.Agr_Persona.Modtabla.setRowCount(0);//en ves de limpiar y poner otra vez la lista en la tabla, lo que voya a hacer despues es que me recorra la tabla al igual que el arraylist de arriba y luego me reemplaze esa persona, pero primero analizar, ya que uno me a reemplazar todos los datos, pero la otra forma me va a recorrer todos los datos, alguno debe ser mejor, despues veo...
                    JFrame_Principal.Agr_Persona.Llenar_Tabla();
                    PersonaDAO.Actualizar(objPersona);
                    
                }
            }   
        } catch (Exception e) {
        }
        JFrame_Principal.Fondo.dispose();
        dispose();
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txt_Id = new Componentes.Textfield_gen();
        txt_Apel_Paterno = new Componentes.Textfield_gen();
        txt_Apel_Materno = new Componentes.Textfield_gen();
        txt_DNI = new Componentes.Textfield_gen();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cbo_Genero = new Componentes.cbo();
        txt_Nombre = new Componentes.Textfield_gen();
        jLabel8 = new javax.swing.JLabel();
        button_gen2 = new Componentes.Button_gen();
        button_gen1 = new Componentes.Button_gen();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(4, 50, 57));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_Id.setEditable(false);
        txt_Id.setPlaceholder("ID");
        jPanel1.add(txt_Id, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 30, 80, -1));

        txt_Apel_Paterno.setPlaceholder("Ingrese apellido paterno.");
        txt_Apel_Paterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_Apel_PaternoKeyPressed(evt);
            }
        });
        jPanel1.add(txt_Apel_Paterno, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 130, -1, -1));

        txt_Apel_Materno.setPlaceholder("Ingrese apellido materno.");
        txt_Apel_Materno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_Apel_MaternoKeyPressed(evt);
            }
        });
        jPanel1.add(txt_Apel_Materno, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 170, -1, -1));

        txt_DNI.setPlaceholder("Ingrese DNI.");
        txt_DNI.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txt_DNIMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txt_DNIMousePressed(evt);
            }
        });
        txt_DNI.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_DNIKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_DNIKeyTyped(evt);
            }
        });
        jPanel1.add(txt_DNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 210, -1, -1));

        jLabel2.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("Modificar Persona.");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 240, 40));

        jLabel3.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Apellido Paterno:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 160, 30));

        jLabel4.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Apellido Materno:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 160, 30));

        jLabel5.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("D.N.I:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, 160, 30));

        jLabel6.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Género:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, 120, 30));

        jLabel7.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("ID:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 30, 60, 30));

        cbo_Genero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Femenino" }));
        jPanel1.add(cbo_Genero, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 250, -1, -1));

        txt_Nombre.setPlaceholder("Ingrese nombre.");
        txt_Nombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txt_NombreMouseEntered(evt);
            }
        });
        txt_Nombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_NombreKeyPressed(evt);
            }
        });
        jPanel1.add(txt_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 90, -1, -1));

        jLabel8.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Nombre:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 120, 30));

        button_gen2.setText("Eliminar");
        button_gen2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen2ActionPerformed(evt);
            }
        });
        jPanel1.add(button_gen2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 320, 140, -1));

        button_gen1.setText("Modificar");
        button_gen1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen1ActionPerformed(evt);
            }
        });
        jPanel1.add(button_gen1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 320, 140, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void button_gen1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen1ActionPerformed
        Modificar();
    }//GEN-LAST:event_button_gen1ActionPerformed

    private void txt_DNIMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_DNIMousePressed
        
    }//GEN-LAST:event_txt_DNIMousePressed

    private void txt_DNIKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_DNIKeyPressed
        if(evt.getKeyChar()==KeyEvent.VK_ENTER)
        {
            Modificar();
        }
    }//GEN-LAST:event_txt_DNIKeyPressed

    private void txt_DNIKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_DNIKeyTyped
        Val_Keytiped.keyTyped_int(evt, txt_DNI);        
    }//GEN-LAST:event_txt_DNIKeyTyped

    private void button_gen2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen2ActionPerformed
        Cargar_persona();
        
        try 
        {
            for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
            {
                if(JFrame_Principal.Lista.get(i).getId()==Integer.parseInt(txt_Id.getText()))
                {
                    JFrame_Principal.Lista.remove(i);
                    JFrame_Principal.Agr_Persona.Modtabla.setRowCount(0);//en ves de limpiar y poner otra vez la lista en la tabla, lo que voya a hacer despues es que me recorra la tabla al igual que el arraylist de arriba y luego me reemplaze esa persona, pero primero analizar, ya que uno me a reemplazar todos los datos, pero la otra forma me va a recorrer todos los datos, alguno debe ser mejor, despues veo...
                    JFrame_Principal.Agr_Persona.Llenar_Tabla();
                    PersonaDAO.Eliminar(objPersona);
                    
                }
            }   
        } catch (Exception e) {
        }
        JFrame_Principal.Fondo.dispose();
        dispose();
    }//GEN-LAST:event_button_gen2ActionPerformed

    private void txt_DNIMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_DNIMouseEntered
        
    }//GEN-LAST:event_txt_DNIMouseEntered

    private void txt_NombreMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_NombreMouseEntered
        
    }//GEN-LAST:event_txt_NombreMouseEntered

    private void txt_Apel_PaternoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_Apel_PaternoKeyPressed
        if(evt.getKeyChar()==KeyEvent.VK_ENTER)
        {
            Modificar();
        }
    }//GEN-LAST:event_txt_Apel_PaternoKeyPressed

    private void txt_Apel_MaternoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_Apel_MaternoKeyPressed
        if(evt.getKeyChar()==KeyEvent.VK_ENTER)
        {
            Modificar();
        }
    }//GEN-LAST:event_txt_Apel_MaternoKeyPressed

    private void txt_NombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_NombreKeyPressed
        if(evt.getKeyChar()==KeyEvent.VK_ENTER)
        {
            Modificar();
        }
    }//GEN-LAST:event_txt_NombreKeyPressed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        
        JFrame_Incio_sesion.jf.Salir();
        JFrame_Incio_sesion.jf.dispose();
    }//GEN-LAST:event_formWindowClosing

    public Persona getObjPersona() {
        return objPersona;
    }

    public void setObjPersona(Persona objPersona) {
        this.objPersona = objPersona;
    }

    public cbo getCbo_Genero() {
        return cbo_Genero;
    }

    public void setCbo_Genero(cbo cbo_Genero) {
        this.cbo_Genero = cbo_Genero;
    }

    public Textfield_gen getTxt_Apel_Materno() {
        return txt_Apel_Materno;
    }

    public void setTxt_Apel_Materno(Textfield_gen txt_Apel_Materno) {
        this.txt_Apel_Materno = txt_Apel_Materno;
    }

    public Textfield_gen getTxt_Apel_Paterno() {
        return txt_Apel_Paterno;
    }

    public void setTxt_Apel_Paterno(Textfield_gen txt_Apel_Paterno) {
        this.txt_Apel_Paterno = txt_Apel_Paterno;
    }

    public Textfield_gen getTxt_DNI() {
        return txt_DNI;
    }

    public void setTxt_DNI(Textfield_gen txt_DNI) {
        this.txt_DNI = txt_DNI;
    }

    public Textfield_gen getTxt_Id() {
        return txt_Id;
    }

    public void setTxt_Id(Textfield_gen txt_Id) {
        this.txt_Id = txt_Id;
    }

    public Textfield_gen getTxt_Nombre() {
        return txt_Nombre;
    }

    public void setTxt_Nombre(Textfield_gen txt_Nombre) {
        this.txt_Nombre = txt_Nombre;
    }

    
    

    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mod_perso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mod_perso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mod_perso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mod_perso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                mod_perso dialog = new mod_perso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Componentes.Button_gen button_gen1;
    private Componentes.Button_gen button_gen2;
    private Componentes.cbo cbo_Genero;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private Componentes.Textfield_gen txt_Apel_Materno;
    private Componentes.Textfield_gen txt_Apel_Paterno;
    private Componentes.Textfield_gen txt_DNI;
    private Componentes.Textfield_gen txt_Id;
    private Componentes.Textfield_gen txt_Nombre;
    // End of variables declaration//GEN-END:variables
}
