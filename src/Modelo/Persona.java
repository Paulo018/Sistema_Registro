package Modelo;
public class Persona 
{   
    private int Id;
    private String Nombre;
    private String Apel_Paterno;
    private String Apel_Materno;
    private String Nombre_Completo;
    private int DNI, Genero;

    public Persona(int Id, String Nombre, String Apel_Paterno, String Apel_Materno, String Nombre_Completo, int DNI, int Genero) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Apel_Paterno = Apel_Paterno;
        this.Apel_Materno = Apel_Materno;
        this.Nombre_Completo = Nombre_Completo;
        this.DNI = DNI;
        this.Genero = Genero;
    }

    
    public Persona(int Id, String Nombre, String Apel_Paterno, String Apel_Materno, int DNI) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Apel_Paterno = Apel_Paterno;
        this.Apel_Materno = Apel_Materno;
        this.DNI = DNI;
    }
    
    

    public Persona(int Id, String Nombre_Completo) {
        this.Id = Id;
        this.Nombre_Completo = Nombre_Completo;
    }
    

    public Persona() {
    }

    
    
    
    public Object[] getRegistro()
    {
        return new Object[]{Id, Nombre_Completo, DNI};
    }
    

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApel_Paterno() {
        return Apel_Paterno;
    }

    public void setApel_Paterno(String Apel_Paterno) {
        this.Apel_Paterno = Apel_Paterno;
    }

    public String getApel_Materno() {
        return Apel_Materno;
    }

    public void setApel_Materno(String Apel_Materno) {
        this.Apel_Materno = Apel_Materno;
    }

    public int getDNI() {
        return DNI;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }

    public String getNombre_Completo() {
        return Nombre_Completo;
    }

    public void setNombre_Completo(String Nombre_Completo) {
        this.Nombre_Completo = Nombre_Completo;
    }

    public int getGenero() {
        return Genero;
    }

    public void setGenero(int Genero) {
        this.Genero = Genero;
    }
    
    


    
    
}
