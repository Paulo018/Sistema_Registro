package Modelo;

public class Rol_Usuario
{
    int Id_Usuario, Id_Rol;
    public Rol_Usuario(int Id, int Id_Rol) {
        this.Id_Usuario = Id;
        this.Id_Rol = Id_Rol;
    }

    public int getId_Usuario() {
        return Id_Usuario;
    }

    public void setId_Usuario(int Id_Usuario) {
        this.Id_Usuario = Id_Usuario;
    }

    public int getId_Rol() {
        return Id_Rol;
    }

    public void setId_Rol(int Id_Rol) {
        this.Id_Rol = Id_Rol;
    }
    
    
}
