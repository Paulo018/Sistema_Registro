package Modelo;

public class Usuario 
{
    int Id, Id_Persona;
    String Nickname, Password;
    String Nombre_Completo;
    int DNI;

    public Usuario(int Id_Persona, String Nickname, String Password) {
        this.Id_Persona = Id_Persona;
        this.Nickname = Nickname;
        this.Password = Password;
    }

    public Usuario(int Id, int Id_Persona, String Nickname, String Password, int Id_Rol) {
        this.Id = Id;
        this.Id_Persona = Id_Persona;
        this.Nickname = Nickname;
        this.Password = Password;
    }

        
    public Usuario() {
    }
    
    
    public Object[] getRegistro()
    {
        return new Object[]{Id, Nickname, Nombre_Completo, DNI};
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getId_Persona() {
        return Id_Persona;
    }

    public void setId_Persona(int Id_Persona) {
        this.Id_Persona = Id_Persona;
    }

    public String getNickname() {
        return Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }


    public String getNombre_Completo() {
        return Nombre_Completo;
    }

    public void setNombre_Completo(String Nombre_Completo) {
        this.Nombre_Completo = Nombre_Completo;
    }

    public int getDNI() {
        return DNI;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }
    
    
    
    
    
}
