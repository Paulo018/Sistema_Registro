package Paneles;

import Componentes.Textfield_gen;
import ControlDAO.PersonaDAO;
import Modelo.Persona;
import Validaciones.Val_Keytiped;
import Vista.JFrame_Principal;
import static Vista.JFrame_Principal.Fondo;
import static Vista.JFrame_Principal.mod_perso;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;

public final class pnl_Agregar_Persona extends javax.swing.JPanel
{
    Persona objPersona;
    public DefaultTableModel Modtabla;
    PersonaDAO obj_PersonaDAO;
    Val_Keytiped val_Key;

    public pnl_Agregar_Persona()
    {
        initComponents();
        Llenar_Tabla();
        tbl_Persona.getParent().setBackground(new Color(4,50,57));
        val_Key = new Val_Keytiped();
        obj_PersonaDAO= new PersonaDAO();
        Modtabla = (DefaultTableModel) tbl_Persona.getModel();
        txt_Nombre.requestFocus();
        txt_Nombre.selectAll();
    }

    private void Limpiar_txt()
    {
        txt_Nombre.setText("");
        txt_Apel_Paterno.setText("");
        txt_Apel_Materno.setText("");
        txt_DNI.setText("");
        cbo_Genero.setSelectedIndex(0);
    }

    public Textfield_gen getTxt_Nombre() {
        return txt_Nombre;
    }

    public void setTxt_Nombre(Textfield_gen txt_Nombre) {
        this.txt_Nombre = txt_Nombre;
    }
    
    public Persona Cargar_persona()
    {
        String Nombre = txt_Nombre.getText();
        String Apel_Pat = txt_Apel_Paterno.getText();
        String Apel_Mat = txt_Apel_Materno.getText();
        String Slug = txt_Nombre.getText()+ " " +txt_Apel_Paterno.getText()+ " " +txt_Apel_Materno.getText();
        int DNI = Integer.parseInt(txt_DNI.getText()+"");
        int Genero = cbo_Genero.getSelectedIndex();
        
        int ID=0;
        if(JFrame_Principal.Lista.isEmpty())
        {
            ID = 1;
        }
        else
        {
            ID = JFrame_Principal.Lista.get(JFrame_Principal.Lista.size()-1).getId()+1;
        }
        return objPersona = new Persona(ID ,Nombre, Apel_Pat, Apel_Mat, Slug, DNI, Genero);
    }
    
    
    public void Llenar_Tabla() 
    {
        for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
        {
            Modtabla.addRow(JFrame_Principal.Lista.get(i).getRegistro());
        }
    }
    
    public void Llenar_Tabla_1() 
    {
        Modtabla =(DefaultTableModel) tbl_Persona.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_Nombre = new Componentes.Textfield_gen();
        txt_Apel_Paterno = new Componentes.Textfield_gen();
        txt_Apel_Materno = new Componentes.Textfield_gen();
        txt_DNI = new Componentes.Textfield_gen();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cbo_Genero = new Componentes.cbo();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_Persona = new Componentes.Tabla();
        button_gen1 = new Componentes.Button_gen();

        setBackground(new java.awt.Color(0, 80, 80));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_Nombre.setPlaceholder("Ingrese nombre.");
        add(txt_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 80, -1, -1));

        txt_Apel_Paterno.setPlaceholder("Ingrese apellido paterno.");
        add(txt_Apel_Paterno, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 120, -1, -1));

        txt_Apel_Materno.setPlaceholder("Ingrese apellido materno.");
        add(txt_Apel_Materno, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 160, -1, -1));

        txt_DNI.setPlaceholder("Ingrese DNI.");
        txt_DNI.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_DNIKeyTyped(evt);
            }
        });
        add(txt_DNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 200, -1, -1));

        jLabel1.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Ingresar Persona.");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 300, 40));

        jLabel2.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Apellido Paterno:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 160, 30));

        jLabel3.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Apellido Materno:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 160, 30));

        jLabel4.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("D.N.I:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 160, 30));

        jLabel5.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Género:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, 120, 30));

        jLabel6.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Nombre:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, 120, 30));

        cbo_Genero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Femenino" }));
        cbo_Genero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbo_GeneroActionPerformed(evt);
            }
        });
        add(cbo_Genero, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 240, -1, -1));

        jPanel1.setBackground(new java.awt.Color(4, 50, 57));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setBackground(new java.awt.Color(0, 102, 102));

        tbl_Persona.setBackground(new java.awt.Color(0, 153, 153));
        tbl_Persona.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre Completo", "D.N.I"
            }
        ));
        tbl_Persona.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        tbl_Persona.setDoubleBuffered(true);
        tbl_Persona.setRequestFocusEnabled(false);
        tbl_Persona.setUpdateSelectionOnSort(false);
        tbl_Persona.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                tbl_PersonaMouseDragged(evt);
            }
        });
        tbl_Persona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbl_PersonaMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_Persona);
        if (tbl_Persona.getColumnModel().getColumnCount() > 0) {
            tbl_Persona.getColumnModel().getColumn(0).setPreferredWidth(40);
            tbl_Persona.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_Persona.getColumnModel().getColumn(2).setPreferredWidth(110);
        }

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, 390, 410));

        button_gen1.setText("Agregar");
        button_gen1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen1ActionPerformed(evt);
            }
        });
        jPanel1.add(button_gen1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 300, 140, -1));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 830, 500));
    }// </editor-fold>//GEN-END:initComponents

    private void button_gen1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen1ActionPerformed
        try 
        {
            Cargar_persona();
            JFrame_Principal.Lista.add(objPersona);
            PersonaDAO.Insertar(objPersona);
            Limpiar_txt();
            
            Modtabla.addRow(objPersona.getRegistro());
            
        } catch (Exception ex) 
        {
        }
    }//GEN-LAST:event_button_gen1ActionPerformed

    private void txt_DNIKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_DNIKeyTyped
        val_Key.keyTyped_int(evt, txt_DNI);
    }//GEN-LAST:event_txt_DNIKeyTyped

    private void cbo_GeneroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbo_GeneroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbo_GeneroActionPerformed

    private void tbl_PersonaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_PersonaMousePressed
        Fondo.setVisible(true);
        //Fondo.setLocationRelativeTo(Fondo.getParent());// funciona porque su padre se encuentra en el jFrame_Principal.
        Fondo.setLocation(Fondo.getParent().getLocation().x, Fondo.getParent().getLocation().y);
        for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
        {
            if(JFrame_Principal.Lista.get(i).getId()== Integer.parseInt(Modtabla.getValueAt(Integer.parseInt(tbl_Persona.getSelectedRow()+""), 0)+""))
            {
                mod_perso.getTxt_Id().setText(JFrame_Principal.Lista.get(i).getId()+"");
                mod_perso.getTxt_Nombre().setText(JFrame_Principal.Lista.get(i).getNombre()+"");
                mod_perso.getTxt_Apel_Materno().setText(JFrame_Principal.Lista.get(i).getApel_Materno()+"");
                mod_perso.getTxt_Apel_Paterno().setText(JFrame_Principal.Lista.get(i).getApel_Paterno()+"");
                mod_perso.getTxt_DNI().setText(JFrame_Principal.Lista.get(i).getDNI()+"");
                mod_perso.getCbo_Genero().setSelectedIndex(JFrame_Principal.Lista.get(i).getGenero());
            }
        }
        mod_perso.setVisible(true);
////        mod_perso.setLocationRelativeTo(mod_perso.getParent());
        mod_perso.setLocation(mod_perso.getParent().getLocation().x+275, mod_perso.getParent().getLocation().y+75);
    }//GEN-LAST:event_tbl_PersonaMousePressed

    private void tbl_PersonaMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_PersonaMouseDragged
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_PersonaMouseDragged

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Componentes.Button_gen button_gen1;
    private Componentes.cbo cbo_Genero;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private Componentes.Tabla tbl_Persona;
    private Componentes.Textfield_gen txt_Apel_Materno;
    private Componentes.Textfield_gen txt_Apel_Paterno;
    private Componentes.Textfield_gen txt_DNI;
    private Componentes.Textfield_gen txt_Nombre;
    // End of variables declaration//GEN-END:variables
}
