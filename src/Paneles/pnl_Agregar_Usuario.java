package Paneles;

import Componentes.cbo;
import Componentes.Textfield_gen;
import ControlDAO.PersonaDAO;
import ControlDAO.RolDAO;
import ControlDAO.UsuarioDAO;
import Modelo.Persona;
import Modelo.Usuario;
import Validaciones.Val_Keytiped;
import Vista.JFrame_Incio_sesion;
import Vista.JFrame_Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;

public class pnl_Agregar_Usuario extends javax.swing.JPanel
{
    
    Usuario objUsuario;Persona objPersona;
    Val_Keytiped val_key;
    public DefaultComboBoxModel<String> Mod_cbo;
    PersonaDAO obj_personaDAO;
    RolDAO obj_RolDAO;

    pnl_Agregar_Persona pnl_Agr_per;
    public pnl_Agregar_Usuario()
    {
        initComponents();
        val_key = new Val_Keytiped();
        Mod_cbo = (DefaultComboBoxModel<String>) cbo_Persona.getModel();
        obj_personaDAO = new PersonaDAO();
        pnl_Agr_per = new pnl_Agregar_Persona();
        txt_Usuario.requestFocus();
//        Mod_cbo_Rol = (DefaultComboBoxModel<String>) cbo_Rol.getModel();
        
    }

    private void Limpiar_txt()
    {
        txt_Usuario.setText("");
        psw_password_confirm.setText("");
        psw_password.setText("");
    }
    
    
    public void llenar_ComboBox_ID()
    {
        for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
        {
            Mod_cbo.addElement(JFrame_Principal.Lista.get(i).getNombre_Completo());
        }
        if(Mod_cbo.getSize()==0)
        {
            txt_Id.setText(0+"");
        }   
    }
    
    public void Cargar_Usuario()
    {
        int ID = Integer.parseInt(txt_Id.getText());
        String Usuario = txt_Usuario.getText();
        char[] arrayC = psw_password.getPassword();
        String pass = new String(arrayC);
//        int rol =cbo_Rol.getSelectedIndex()+1;
        objUsuario = new Usuario(ID, Usuario, pass);
        try {
            UsuarioDAO.Insertar(objUsuario);
            JFrame_Incio_sesion.Lista_usu.Agregar(objUsuario);
        } catch (Exception ex) {
            Logger.getLogger(pnl_Agregar_Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_Usuario = new Componentes.Textfield_gen();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        button_gen1 = new Componentes.Button_gen();
        jLabel5 = new javax.swing.JLabel();
        psw_password_confirm = new Componentes.Password_gen();
        psw_password = new Componentes.Password_gen();
        txt_Id = new Componentes.Textfield_gen();
        jLabel7 = new javax.swing.JLabel();
        cbo_Persona = new Componentes.cbo();
        jLabel8 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(4, 50, 57));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_Usuario.setPlaceholder("Ingrese nombre de Usuario.");
        txt_Usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_UsuarioKeyTyped(evt);
            }
        });
        add(txt_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 190, -1, -1));

        jLabel1.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Agregar Nuevo Usuario.");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 300, 40));

        jLabel2.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Contraseña:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, 160, 30));

        jLabel3.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Confirmar Contraseña:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, 190, 30));

        button_gen1.setText("Agregar");
        button_gen1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen1ActionPerformed(evt);
            }
        });
        add(button_gen1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 380, 140, -1));

        jLabel5.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("ID:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, 50, 30));

        psw_password_confirm.setPlaceholder("Confirme Contraseña.");
        add(psw_password_confirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 310, -1, -1));

        psw_password.setFocusAccelerator('.');
        psw_password.setPlaceholder("Ingrese Contraseña.");
        add(psw_password, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 270, -1, -1));

        txt_Id.setEditable(false);
        txt_Id.setPlaceholder("ID.");
        add(txt_Id, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 100, 90, -1));

        jLabel7.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Selecciona a la Persona:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 200, 30));

        cbo_Persona.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                cbo_PersonaMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                cbo_PersonaMouseMoved(evt);
            }
        });
        cbo_Persona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbo_PersonaMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cbo_PersonaMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cbo_PersonaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                cbo_PersonaMouseReleased(evt);
            }
        });
        cbo_Persona.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                cbo_PersonaComponentMoved(evt);
            }
        });
        cbo_Persona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbo_PersonaActionPerformed(evt);
            }
        });
        add(cbo_Persona, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 100, 290, -1));

        jLabel8.setFont(new java.awt.Font("Open Sans", 1, 15)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Usuario:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, 120, 30));
    }// </editor-fold>//GEN-END:initComponents

    private void button_gen1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen1ActionPerformed
        Cargar_Usuario();
        Limpiar_txt();
    }//GEN-LAST:event_button_gen1ActionPerformed

    private void txt_UsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_UsuarioKeyTyped
        val_key.keyTyped_sin_space(evt, txt_Usuario);
    }//GEN-LAST:event_txt_UsuarioKeyTyped

    private void cbo_PersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbo_PersonaActionPerformed
        for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
        {
            if(Mod_cbo.getSelectedItem()==JFrame_Principal.Lista.get(i).getNombre_Completo())
            {
                txt_Id.setText(JFrame_Principal.Lista.get(i).getId()+ ""); 
            }
        }
    }//GEN-LAST:event_cbo_PersonaActionPerformed

    private void cbo_PersonaMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbo_PersonaMouseDragged
        
    }//GEN-LAST:event_cbo_PersonaMouseDragged

    private void cbo_PersonaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbo_PersonaMouseReleased
       
    }//GEN-LAST:event_cbo_PersonaMouseReleased

    private void cbo_PersonaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbo_PersonaMousePressed
    }//GEN-LAST:event_cbo_PersonaMousePressed

    private void cbo_PersonaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbo_PersonaMouseExited
       // TODO add your handling code here:
    }//GEN-LAST:event_cbo_PersonaMouseExited

    private void cbo_PersonaMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbo_PersonaMouseMoved

    }//GEN-LAST:event_cbo_PersonaMouseMoved

    private void cbo_PersonaComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_cbo_PersonaComponentMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_cbo_PersonaComponentMoved

    private void cbo_PersonaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbo_PersonaMouseClicked

    }//GEN-LAST:event_cbo_PersonaMouseClicked

    public Textfield_gen getTxt_Id() {
        return txt_Id;
    }

    public void setTxt_Id(Textfield_gen txt_Id) {
        this.txt_Id = txt_Id;
    }

    
    
    public DefaultComboBoxModel<String> getMod_cbo() {
        return Mod_cbo;
    }

    public void setMod_cbo(DefaultComboBoxModel<String> Mod_cbo) {
        this.Mod_cbo = Mod_cbo;
   }

    public cbo getCbo_Persona() {
        return cbo_Persona;
    }

    public void setCbo_Persona(cbo cbo_Persona) {
        this.cbo_Persona = cbo_Persona;
    }
    

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Componentes.Button_gen button_gen1;
    private Componentes.cbo cbo_Persona;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private Componentes.Password_gen psw_password;
    private Componentes.Password_gen psw_password_confirm;
    private Componentes.Textfield_gen txt_Id;
    private Componentes.Textfield_gen txt_Usuario;
    // End of variables declaration//GEN-END:variables
}
