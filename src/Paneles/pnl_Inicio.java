package Paneles;

import Control.Control_Usuario;
import ControlDAO.UsuarioDAO;
import Vista.JFrame_Incio_sesion;
import Vista.JFrame_Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

public class pnl_Inicio extends javax.swing.JPanel
{
    

    public pnl_Inicio() 
    {
        initComponents();
        try {
            for (int i = 0; i < UsuarioDAO.Consultar_todos().size(); i++) 
            {
                Control_Usuario.Lista.add(UsuarioDAO.Consultar_todos().get(i));
                
            }
        } catch (Exception ex) {
            Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JLabel getjLabel1() {
        return jLabel1;
    }

    public void setjLabel1(JLabel jLabel1) {
        this.jLabel1 = jLabel1;
    }

    public JLabel getjLabel2() {
        return jLabel2;
    }

    public void setjLabel2(JLabel jLabel2) {
        this.jLabel2 = jLabel2;
    }

    public JLabel getjLabel6() {
        return jLabel6;
    }

    public void setjLabel6(JLabel jLabel6) {
        this.jLabel6 = jLabel6;
    }

    public JLabel getjLabel7() {
        return lbl_nick;
    }

    public void setjLabel7(JLabel jLabel7) {
        this.lbl_nick = jLabel7;
    }

    public JLabel getjLabel8() {
        return jLabel8;
    }

    public void setjLabel8(JLabel jLabel8) {
        this.jLabel8 = jLabel8;
    }

    public JLabel getLbl_dni() {
        return lbl_dni;
    }

    public void setLbl_dni(JLabel lbl_dni) {
        this.lbl_dni = lbl_dni;
    }

    public JLabel getLbl_nick() {
        return lbl_nick;
    }

    public void setLbl_nick(JLabel lbl_nick) {
        this.lbl_nick = lbl_nick;
    }

    public JLabel getLbl_nombre() {
        return lbl_nombre;
    }

    public void setLbl_nombre(JLabel lbl_nombre) {
        this.lbl_nombre = lbl_nombre;
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        lbl_dni = new javax.swing.JLabel();
        lbl_nombre = new javax.swing.JLabel();
        lbl_nick = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setBackground(new java.awt.Color(0, 50, 50));
        jLabel2.setFont(new java.awt.Font("Open Sans", 1, 30)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 50, 50));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Bienvenido:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 180, 40));

        lbl_dni.setBackground(new java.awt.Color(0, 50, 50));
        lbl_dni.setFont(new java.awt.Font("Open Sans", 1, 16)); // NOI18N
        lbl_dni.setForeground(new java.awt.Color(0, 50, 50));
        lbl_dni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbl_dni.setText("a");
        add(lbl_dni, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 180, 230, 30));

        lbl_nombre.setBackground(new java.awt.Color(0, 50, 50));
        lbl_nombre.setFont(new java.awt.Font("Open Sans", 1, 16)); // NOI18N
        lbl_nombre.setForeground(new java.awt.Color(0, 50, 50));
        lbl_nombre.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbl_nombre.setText("a");
        add(lbl_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 230, 30));

        lbl_nick.setBackground(new java.awt.Color(0, 50, 50));
        lbl_nick.setFont(new java.awt.Font("Open Sans", 1, 22)); // NOI18N
        lbl_nick.setForeground(new java.awt.Color(0, 50, 50));
        lbl_nick.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbl_nick.setText("a");
        add(lbl_nick, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 40, 230, 40));

        jLabel6.setBackground(new java.awt.Color(0, 50, 50));
        jLabel6.setFont(new java.awt.Font("Open Sans", 1, 16)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 50, 50));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Nombre:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 80, 30));

        jLabel8.setBackground(new java.awt.Color(0, 50, 50));
        jLabel8.setFont(new java.awt.Font("Open Sans", 1, 16)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 50, 50));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("D.N.I:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 120, 30));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/paulo_vigo-01.png"))); // NOI18N
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 350, 410, 150));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Logo.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 20, 460, 330));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel lbl_dni;
    private javax.swing.JLabel lbl_nick;
    private javax.swing.JLabel lbl_nombre;
    // End of variables declaration//GEN-END:variables
}
