package Paneles;

import Componentes.Tabla;
import Control.Control_Usuario;
import ControlDAO.UsuarioDAO;
import Vista.JFrame_Incio_sesion;
import Vista.JFrame_Principal;
import static Vista.JFrame_Principal.Fondo;
import static Vista.JFrame_Principal.Ver_usuario;
import static Vista.JFrame_Principal.mod_perso;
import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class pnl_Ver_Usuarios extends javax.swing.JPanel 
{
    public static DefaultTableModel Modtabla;
    
    public pnl_Ver_Usuarios() 
    {
        initComponents();
        tbl_Usuario.getParent().setBackground(new Color(4,50,57));
        Modtabla = (DefaultTableModel) tbl_Usuario.getModel();
        Llenar_Tabla();
    }
    
    public void Llenar_Tabla() 
    {
        Modtabla.setRowCount(0);
                 
//        for (int i = 0; i < Control_Usuario.getLista().size(); i++) 
//        {
//            Modtabla.addRow(JFrame_Incio_sesion.Lista_usu.Lista.get(i).getRegistro());
//        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_Usuario = new Componentes.Tabla();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(4, 50, 57));
        setMaximumSize(new java.awt.Dimension(780, 500));
        setMinimumSize(new java.awt.Dimension(780, 500));
        setPreferredSize(new java.awt.Dimension(780, 500));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbl_Usuario.setBackground(new java.awt.Color(204, 0, 0));
        tbl_Usuario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"nkjllj", null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Nombre de Usuario", "Nombre Completo", "DNI"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Usuario.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tbl_Usuario.setColorFondo(new java.awt.Color(255, 51, 0));
        tbl_Usuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbl_UsuarioMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_Usuario);
        if (tbl_Usuario.getColumnModel().getColumnCount() > 0) {
            tbl_Usuario.getColumnModel().getColumn(0).setMinWidth(40);
            tbl_Usuario.getColumnModel().getColumn(0).setPreferredWidth(40);
            tbl_Usuario.getColumnModel().getColumn(1).setMinWidth(220);
            tbl_Usuario.getColumnModel().getColumn(1).setPreferredWidth(220);
            tbl_Usuario.getColumnModel().getColumn(2).setMinWidth(220);
            tbl_Usuario.getColumnModel().getColumn(2).setPreferredWidth(220);
            tbl_Usuario.getColumnModel().getColumn(3).setMinWidth(200);
            tbl_Usuario.getColumnModel().getColumn(3).setPreferredWidth(200);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 100, 700, 380));

        jLabel1.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Lista de Usuarios.");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 300, 40));
    }// </editor-fold>//GEN-END:initComponents

    private void tbl_UsuarioMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_UsuarioMousePressed
        
        //Fondo.setLocationRelativeTo(Fondo.getParent());// funciona porque su padre se encuentra en el jFrame_Principal.
        Fondo.setLocation(Fondo.getParent().getLocation().x, Fondo.getParent().getLocation().y);
        JFrame_Principal.asignar_Roles.getjLayeredPane1().removeAll();
        JFrame_Principal.asignar_Roles.crear_Checkbox();
        for (int i = 0; i < JFrame_Incio_sesion.Lista_usu.Lista.size(); i++) 
        {
            if(Control_Usuario.Lista.get(i).getId()== Integer.parseInt(Modtabla.getValueAt(Integer.parseInt(tbl_Usuario.getSelectedRow()+""), 0)+""))
            {
                JFrame_Principal.asignar_Roles.getTxt_Id().setText(Control_Usuario.Lista.get(i).getId()+"");
            }
        }Fondo.setVisible(true);
        JFrame_Principal.asignar_Roles.setVisible(true);
////        mod_perso.setLocationRelativeTo(mod_perso.getParent());
        JFrame_Principal.asignar_Roles.setLocation(JFrame_Principal.asignar_Roles.getParent().getLocation().x+275, JFrame_Principal.asignar_Roles.getParent().getLocation().y+75);
    }//GEN-LAST:event_tbl_UsuarioMousePressed

    public Tabla getTbl_Usuario() {
        return tbl_Usuario;
    }

    public void setTbl_Usuario(Tabla tbl_Usuario) {
        this.tbl_Usuario = tbl_Usuario;
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private Componentes.Tabla tbl_Usuario;
    // End of variables declaration//GEN-END:variables
}
