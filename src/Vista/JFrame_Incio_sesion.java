package Vista;

import Componentes.JCheckBoxCustomIcon;
import Componentes.Textfield_gen;
import Control.Control_Usuario;
import ControlDAO.RolDAO;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
public class JFrame_Incio_sesion extends javax.swing.JFrame
{
    int x, y;
    
    public static JFrame_Principal jf;// me parece que lo mejor es inicializar al hacer click en aceptar
    // pero de esta forma inicializa rapido el programa
    public static Control_Usuario Lista_usu;
    
    public JFrame_Incio_sesion() 
    {
        setAlwaysOnTop(true);
        setUndecorated(true);
        initComponents();
        setLocationRelativeTo(null);
        Lista_usu= new Control_Usuario();
        jf= new JFrame_Principal();
        Lista_usu.Cargar_de_bd();
        txt_Usuario.requestFocus();
    }
    
    
    
    private  void Ingresar()
    {
       
        try 
        {
            for (int i = 0; i < Control_Usuario.Lista.size(); i++) {
                
                if(Control_Usuario.Lista.get(i).getNickname().equals(txt_Usuario.getText()))
                {
                    jf.setVisible(true);
                    
                    // con esto haré que me llene los datos en el panel de Inicio.
                    jf.Inicio.getLbl_nick().setText(Control_Usuario.Lista.get(i).getNickname());
                    for (int j = 0; j < JFrame_Principal.Lista.size(); j++)
                    
                    {
                    if(JFrame_Principal.Lista.get(j).getId()==(Control_Usuario.Lista.get(i).getId_Persona()))
                        {                   
                            // con esto haré que me llene los datos en el panel de Inicio.
                            jf.Inicio.getLbl_dni().setText(JFrame_Principal.Lista.get(j).getDNI()+"");
                            jf.Inicio.getLbl_nombre().setText(JFrame_Principal.Lista.get(j).getNombre_Completo()+"");
                        }
                    }
                    setVisible(false);
                }
            }
            
//                for (int j = 0; j < Control_Usuario.Lista.size(); j++) 
//                {for (int i = 0; i < JFrame_Principal.Lista.size(); i++) 
//            {
//                    if(JFrame_Principal.Lista.get(i).getId()==(Control_Usuario.Lista.get(j).getId_Persona()))
//                    {                   
//                        // con esto haré que me llene los datos en el panel de Inicio.
//                        jf.Inicio.getLbl_dni().setText(JFrame_Principal.Lista.get(i).getDNI()+"");
//                        jf.Inicio.getLbl_nombre().setText(JFrame_Principal.Lista.get(i).getNombre_Completo()+"");
//                    }
//                }
//            }
            
        } catch (Exception ex) {            
            Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_Img = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        lbl_Contrasena = new javax.swing.JLabel();
        txt_Usuario = new Componentes.Textfield_gen();
        button_gen1 = new Componentes.Button_gen();
        button_gen2 = new Componentes.Button_gen();
        password_gen1 = new Componentes.Password_gen();
        lbl_Usuario1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        lbl_Usuario = new javax.swing.JLabel();
        lbl_Usuario2 = new javax.swing.JLabel();

        lbl_Img.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Perfil-02.png"))); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage("src/Imagenes/Logo_min_1.png"));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 129, 83));
        jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel1MouseDragged(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel1MousePressed(evt);
            }
        });
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel8.setBackground(new java.awt.Color(0, 40, 40));
        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, 10));

        jPanel7.setBackground(new java.awt.Color(0, 25, 25));
        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, 10));

        jPanel9.setBackground(new java.awt.Color(0, 40, 40));
        jPanel1.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 300));

        jPanel4.setBackground(new java.awt.Color(0, 40, 40));
        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 750, 10));

        lbl_Contrasena.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Contrasena.setFont(new java.awt.Font("Myriad Pro", 1, 20)); // NOI18N
        lbl_Contrasena.setForeground(new java.awt.Color(255, 255, 255));
        lbl_Contrasena.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_Contrasena.setText("Contraseña :");
        lbl_Contrasena.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.add(lbl_Contrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 160, 120, 30));

        txt_Usuario.setText("Paulo0132");
        txt_Usuario.setPlaceholder("Escriba su Usuario.");
        txt_Usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_UsuarioKeyPressed(evt);
            }
        });
        jPanel1.add(txt_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 110, -1, 30));

        button_gen1.setText("Ingresar");
        button_gen1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen1ActionPerformed(evt);
            }
        });
        jPanel1.add(button_gen1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 240, 400, 50));

        button_gen2.setText("Cancelar");
        button_gen2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen2ActionPerformed(evt);
            }
        });
        jPanel1.add(button_gen2, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 10, 400, 50));

        password_gen1.setPlaceholder("Escriba su Contraseña.");
        jPanel1.add(password_gen1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 160, -1, -1));

        lbl_Usuario1.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Usuario1.setFont(new java.awt.Font("Myriad Pro", 1, 20)); // NOI18N
        lbl_Usuario1.setForeground(new java.awt.Color(255, 255, 255));
        lbl_Usuario1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_Usuario1.setText("Usuario :");
        lbl_Usuario1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.add(lbl_Usuario1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 110, 90, 30));

        jPanel6.setBackground(new java.awt.Color(10, 78, 63));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_Usuario.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Usuario.setFont(new java.awt.Font("Myriad Pro", 1, 36)); // NOI18N
        lbl_Usuario.setForeground(new java.awt.Color(255, 255, 255));
        lbl_Usuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Usuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Logo_min_4.png"))); // NOI18N
        lbl_Usuario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel6.add(lbl_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 340, 240));

        lbl_Usuario2.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Usuario2.setFont(new java.awt.Font("Myriad Pro", 1, 36)); // NOI18N
        lbl_Usuario2.setForeground(new java.awt.Color(255, 255, 255));
        lbl_Usuario2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Usuario2.setText("Inicio de sesión");
        lbl_Usuario2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel6.add(lbl_Usuario2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 340, 50));

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 350, 300));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - x, point.y - y);
    }//GEN-LAST:event_jPanel1MouseDragged

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        if (evt.getClickCount() == 2) // cuando se hace click dos veces
        {
            setLocationRelativeTo(null);
        }
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed
        // me capturan la posicion que presiono con el mouse
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel1MousePressed

    private void button_gen1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen1ActionPerformed
        Ingresar();
    }//GEN-LAST:event_button_gen1ActionPerformed

    private void txt_UsuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_UsuarioKeyPressed
        if(evt.getKeyChar()==KeyEvent.VK_ENTER)
        {
            Ingresar();
        }
    }//GEN-LAST:event_txt_UsuarioKeyPressed

    private void button_gen2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen2ActionPerformed
        System.exit(0);
//        char[] arrayC = password_gen1.getPassword();
//        String pass = new String(arrayC); 
//        txt_Usuario.setText(pass);

    }//GEN-LAST:event_button_gen2ActionPerformed

    public Textfield_gen getTxt_Usuario() {
        return txt_Usuario;
    }

    public void setTxt_Usuario(Textfield_gen txt_Usuario) {
        this.txt_Usuario = txt_Usuario;
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrame_Incio_sesion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Componentes.Button_gen button_gen1;
    private Componentes.Button_gen button_gen2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JLabel lbl_Contrasena;
    private javax.swing.JLabel lbl_Img;
    private javax.swing.JLabel lbl_Usuario;
    private javax.swing.JLabel lbl_Usuario1;
    private javax.swing.JLabel lbl_Usuario2;
    private Componentes.Password_gen password_gen1;
    private Componentes.Textfield_gen txt_Usuario;
    // End of variables declaration//GEN-END:variables
}
