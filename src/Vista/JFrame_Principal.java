package Vista;

import ControlDAO.PersonaDAO;
import ControlDAO.UsuarioDAO;
import Paneles.pnl_Inicio;
import Hilo.traslacion;
import Jdialogs.Asignar_Roles;
import Jdialogs.Fondo;
import Jdialogs.Notificacion;
import Jdialogs.mod_perso;
import Modelo.Persona;
import Paneles.pnl_Agregar_Persona;
import Paneles.pnl_Agregar_Usuario;
import Paneles.pnl_Ver_Usuarios;
import Paneles.pnl_mas;
import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class JFrame_Principal extends javax.swing.JFrame 
{
    JFrame_Incio_sesion jf;
    Notificacion not ;
    
    PersonaDAO per_DAO; UsuarioDAO usu_DAO;
    
    public static Fondo Fondo;
    
    public static mod_perso mod_perso;
    public static Asignar_Roles asignar_Roles;
    
    public static ArrayList<Persona> Lista;
    
    
    
    public static pnl_Inicio Inicio; public static pnl_Agregar_Persona Agr_Persona; public static pnl_Ver_Usuarios Ver_usuario;pnl_Agregar_Usuario Agr_Usuario; pnl_mas Mas;
    
    public JFrame_Principal()
    {
        setUndecorated(true);
        initComponents();
        Lista = new ArrayList();
        not = new Notificacion(this, false);
        Inicio = new pnl_Inicio();
        Agr_Persona = new pnl_Agregar_Persona();
        Agr_Usuario = new pnl_Agregar_Usuario();
        Mas = new pnl_mas();
        per_DAO = new PersonaDAO();
        usu_DAO = new UsuarioDAO();
        mod_perso= new mod_perso(this, false);
        asignar_Roles = new Asignar_Roles(this, false);
        Ver_usuario = new pnl_Ver_Usuarios();
        Fondo = new Fondo(this, false);
        setLocationRelativeTo(null);
        llenar_Lista();
        Operacion(1);
        Poner_Focos();pnl_Nav.requestFocus();
        
    }
    private int x, y;
    int a = getX();
    private void Operacion(int Numero)
    {
        switch(Numero)
        {   
            case 1: 
                jPanel1.add(Inicio);
                Mostrar_Panel(true, false, false, false, false);
                Poner_Focos();
                break;
            case 2:
                jPanel1.add(Agr_Persona);
                Mostrar_Panel(false, true, false, false, false);
                Agr_Persona.Modtabla.setRowCount(0);
                Agr_Persona.Llenar_Tabla();
                Poner_Focos();
                break;
            case 3:
                jPanel1.add(Agr_Usuario);
                Mostrar_Panel(false, false, true, false, false);
                Agr_Usuario.Mod_cbo.removeAllElements();
                Agr_Usuario.llenar_ComboBox_ID();
                Poner_Focos();
                break;
            case 4:
                jPanel1.add(Ver_usuario);
                        Ver_usuario.Modtabla.setRowCount(0);
                try {
                    for (int i = 0; i < UsuarioDAO.Consultar_todos().size(); i++) 
                    {
                        Ver_usuario.Modtabla.addRow(UsuarioDAO.Consultar_todos().get(i).getRegistro());
                    }
                } catch (Exception ex) {
                    Logger.getLogger(JFrame_Incio_sesion.class.getName()).log(Level.SEVERE, null, ex);
                }
                Mostrar_Panel(false, false, false, true, false);//la tabla de usuario se llena por las instruccione en el constructor...
                break;
            case 5:
                jPanel1.add(Mas);
                Mostrar_Panel(false, false, false, false, true);
                break;
        }
    }
    
    private void Mostrar_Panel(boolean Inicio, boolean Ing_persona, boolean Agr_usuario,boolean Ver_Usuario, boolean Mas)
    {
        this.Inicio.setVisible(Inicio);
        this.Agr_Persona.setVisible(Ing_persona);
        this.Agr_Usuario.setVisible(Agr_usuario);
        this.Ver_usuario.setVisible(Ver_Usuario);
        this.Mas.setVisible(Mas);                
    }
    
    private void Ocultar_not()
    {
        if(not.isVisible()== true)
        {
            not.dispose();not.setVisible(false);
        }
    }
    
    public void Poner_Focos()
    {
        if(Agr_Persona.isVisible())
        {
            Agr_Persona.getTxt_Nombre().requestFocus();
            Agr_Persona.getTxt_Nombre().selectAll();
        }
        if(Agr_Usuario.isVisible())
        {
            Agr_Usuario.getCbo_Persona().requestFocus();
        }
    }
    
    public static void llenar_Lista()
    {
        try {
            Lista = (PersonaDAO.Consultar_todos());
        } catch (Exception ex) {
            Logger.getLogger(JFrame_Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Bloquear_botones(boolean a)
    {
        btn_Inicio.setEnabled(a);
        btn_Agregar_persona.setEnabled(a);
        btn_Agregar_usuario.setEnabled(a);
        btn_Mas_acciones.setEnabled(a);
    }
    
    public void Salir()
    {
        jf= new JFrame_Incio_sesion();
        dispose();
        jf.setVisible(true);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnl_Principal = new javax.swing.JPanel();
        pnl_Nav3 = new javax.swing.JPanel();
        pnl_Nav1 = new javax.swing.JPanel();
        pnl_Nav2 = new javax.swing.JPanel();
        btn_Inicio = new Componentes.Button_gen();
        btn_Agregar_persona = new Componentes.Button_gen();
        btn_Agregar_usuario = new Componentes.Button_gen();
        btn_Mas_acciones1 = new Componentes.Button_gen();
        btn_Mas_acciones = new Componentes.Button_gen();
        pnl_Nav = new javax.swing.JPanel();
        button_gen5 = new Componentes.Button_gen();
        button_gen6 = new Componentes.Button_gen();
        button_gen7 = new Componentes.Button_gen();
        button_gen8 = new Componentes.Button_gen();
        pnl_Nav4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(4, 50, 57));
        setIconImage(Toolkit.getDefaultToolkit().getImage("src/Imagenes/Logo_min_3.png"));
        setMinimumSize(new java.awt.Dimension(1000, 550));
        addWindowStateListener(new java.awt.event.WindowStateListener() {
            public void windowStateChanged(java.awt.event.WindowEvent evt) {
                formWindowStateChanged(evt);
            }
        });
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                formWindowLostFocus(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowDeactivated(java.awt.event.WindowEvent evt) {
                formWindowDeactivated(evt);
            }
            public void windowIconified(java.awt.event.WindowEvent evt) {
                formWindowIconified(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnl_Principal.setBackground(new java.awt.Color(4, 50, 57));
        pnl_Principal.setMaximumSize(new java.awt.Dimension(1000, 550));
        pnl_Principal.setMinimumSize(new java.awt.Dimension(1000, 550));
        pnl_Principal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pnl_PrincipalMousePressed(evt);
            }
        });
        pnl_Principal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnl_Nav3.setBackground(new java.awt.Color(0, 40, 40));
        pnl_Nav3.setMaximumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav3.setMinimumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnl_Principal.add(pnl_Nav3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 540, 1000, 10));

        pnl_Nav1.setBackground(new java.awt.Color(0, 40, 40));
        pnl_Nav1.setMaximumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav1.setMinimumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnl_Principal.add(pnl_Nav1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 10, 550));

        pnl_Nav2.setBackground(new java.awt.Color(0, 73, 83));
        pnl_Nav2.setMaximumSize(new java.awt.Dimension(210, 510));
        pnl_Nav2.setMinimumSize(new java.awt.Dimension(210, 510));
        pnl_Nav2.setPreferredSize(new java.awt.Dimension(210, 510));
        pnl_Nav2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_Inicio.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 10, 1, 1, new java.awt.Color(0, 0, 0)));
        btn_Inicio.setText("Inicio");
        btn_Inicio.setToolTipText("");
        btn_Inicio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_Inicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_InicioActionPerformed(evt);
            }
        });
        pnl_Nav2.add(btn_Inicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 250, 150, 50));

        btn_Agregar_persona.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 10, 1, 1, new java.awt.Color(0, 0, 0)));
        btn_Agregar_persona.setText("Agregar Persona");
        btn_Agregar_persona.setToolTipText("");
        btn_Agregar_persona.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_Agregar_persona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Agregar_personaActionPerformed(evt);
            }
        });
        pnl_Nav2.add(btn_Agregar_persona, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 150, 50));

        btn_Agregar_usuario.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 10, 1, 1, new java.awt.Color(0, 0, 0)));
        btn_Agregar_usuario.setText("Agregar Usuario");
        btn_Agregar_usuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_Agregar_usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Agregar_usuarioActionPerformed(evt);
            }
        });
        pnl_Nav2.add(btn_Agregar_usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 150, 50));

        btn_Mas_acciones1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 10, 1, 1, new java.awt.Color(0, 0, 0)));
        btn_Mas_acciones1.setText("Agregar Roles");
        btn_Mas_acciones1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_Mas_acciones1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Mas_acciones1ActionPerformed(evt);
            }
        });
        pnl_Nav2.add(btn_Mas_acciones1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 150, 50));

        btn_Mas_acciones.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 10, 1, 1, new java.awt.Color(0, 0, 0)));
        btn_Mas_acciones.setText("Más Acciones");
        btn_Mas_acciones.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_Mas_acciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Mas_accionesActionPerformed(evt);
            }
        });
        pnl_Nav2.add(btn_Mas_acciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 450, 150, 50));

        pnl_Principal.add(pnl_Nav2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 150, -1));

        pnl_Nav.setBackground(new java.awt.Color(0, 40, 40));
        pnl_Nav.setMaximumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav.setMinimumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                pnl_NavMouseDragged(evt);
            }
        });
        pnl_Nav.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnl_NavMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pnl_NavMousePressed(evt);
            }
        });
        pnl_Nav.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        button_gen5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sign-out.png"))); // NOI18N
        button_gen5.setText("  Cerrar Sesión");
        button_gen5.setFont(new java.awt.Font("Myriad Pro", 0, 14)); // NOI18N
        button_gen5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen5ActionPerformed(evt);
            }
        });
        pnl_Nav.add(button_gen5, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 0, 140, -1));

        button_gen6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/menu-lines.png"))); // NOI18N
        button_gen6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                button_gen6MousePressed(evt);
            }
        });
        button_gen6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen6ActionPerformed(evt);
            }
        });
        pnl_Nav.add(button_gen6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 50, -1));

        button_gen7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/info.png"))); // NOI18N
        button_gen7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen7ActionPerformed(evt);
            }
        });
        pnl_Nav.add(button_gen7, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 40, -1));

        button_gen8.setText("----");
        button_gen8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_gen8ActionPerformed(evt);
            }
        });
        pnl_Nav.add(button_gen8, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 50, -1));

        pnl_Principal.add(pnl_Nav, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 40));

        pnl_Nav4.setBackground(new java.awt.Color(0, 40, 40));
        pnl_Nav4.setMaximumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav4.setMinimumSize(new java.awt.Dimension(1000, 40));
        pnl_Nav4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnl_Principal.add(pnl_Nav4, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 40, 10, 510));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setLayout(new java.awt.CardLayout());
        pnl_Principal.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 830, 500));

        getContentPane().add(pnl_Principal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pnl_NavMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnl_NavMouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - x, point.y - y);
    }//GEN-LAST:event_pnl_NavMouseDragged

    private void pnl_NavMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnl_NavMouseClicked
        if (evt.getClickCount() == 2)
        {
            setLocationRelativeTo(null);
        }
    }//GEN-LAST:event_pnl_NavMouseClicked

    private void pnl_NavMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnl_NavMousePressed
        x = evt.getX();
        y = evt.getY();
        Ocultar_not();
    }//GEN-LAST:event_pnl_NavMousePressed

    private void btn_InicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_InicioActionPerformed
        Operacion(1);
    }//GEN-LAST:event_btn_InicioActionPerformed

    private void button_gen7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen7ActionPerformed
        not.setLocation(this.getLocation().x+830-230+20, this.getLocation().y+40);
        if(not.isVisible()== false)
        {
            try {
                not.setOpacity(0.95F);
                not.setVisible(true);
            } 
            catch (Exception ex) {}
        }
        else
        {
            Ocultar_not();
        }
    }//GEN-LAST:event_button_gen7ActionPerformed

    private void button_gen6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_button_gen6MousePressed
    }//GEN-LAST:event_button_gen6MousePressed

    private void pnl_PrincipalMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnl_PrincipalMousePressed
        Ocultar_not();
    }//GEN-LAST:event_pnl_PrincipalMousePressed
        
    private void btn_Agregar_personaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Agregar_personaActionPerformed
        Operacion(2);
        
    }//GEN-LAST:event_btn_Agregar_personaActionPerformed

    private void button_gen6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen6ActionPerformed
        try 
        {
                traslacion tr, tr1;
            if(pnl_Nav2.getX()==10)
            {
                tr = new traslacion(pnl_Nav2,-150, 2);
                tr.startAnimation_Horizontal_izq();
//                if(Inicio.isVisible())
//                {
//                    pnl_Principal.setBackground(new Color(255, 255, 255));
//                }
//                else
//                {
//                    pnl_Principal.setBackground(new Color(0, 80, 80));
//                }
                tr1 = new traslacion(jPanel1, 85, 4);
                tr1.startAnimation_Horizontal_izq();
                Bloquear_botones(false);
            }
            if(pnl_Nav2.getX()==-150)
            {
//                if(Inicio.isVisible())
//                {
//                    pnl_Principal.setBackground(new Color(255, 255, 255));
//                }
//                else
//                {
//                    pnl_Principal.setBackground(new Color(0, 80, 80));
//                }
                tr = new traslacion(pnl_Nav2, 10, 2);
                tr.startAnimation_Horizontal_der();
                tr1 = new traslacion(jPanel1, 160, 4);
                tr1.startAnimation_Horizontal_der();
                Bloquear_botones(true);
            }
        } catch (Exception ex) {}
    }//GEN-LAST:event_button_gen6ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        
    }//GEN-LAST:event_formWindowClosed

    private void button_gen5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen5ActionPerformed
        Salir();
    }//GEN-LAST:event_button_gen5ActionPerformed

    private void btn_Agregar_usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Agregar_usuarioActionPerformed
        Operacion(3);        
    }//GEN-LAST:event_btn_Agregar_usuarioActionPerformed

    private void btn_Mas_accionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Mas_accionesActionPerformed
        Operacion(5);
    }//GEN-LAST:event_btn_Mas_accionesActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
    }//GEN-LAST:event_formWindowOpened

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        
        
    }//GEN-LAST:event_formWindowActivated

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        
    }//GEN-LAST:event_formWindowGainedFocus

    private void formWindowIconified(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowIconified
        
    }//GEN-LAST:event_formWindowIconified

    private void formWindowStateChanged(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowStateChanged
        mod_perso.setVisible(false);// al abrir otra vez con el icono de la barra de tareas de windows
        asignar_Roles.setVisible(false);// al abrir otra vez con el icono de la barra de tareas de windows
    }//GEN-LAST:event_formWindowStateChanged

    private void formWindowLostFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowLostFocus
        
       
    }//GEN-LAST:event_formWindowLostFocus

    private void formWindowDeactivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeactivated
        
    }//GEN-LAST:event_formWindowDeactivated

    private void btn_Mas_acciones1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Mas_acciones1ActionPerformed
        Operacion(4);
    }//GEN-LAST:event_btn_Mas_acciones1ActionPerformed

    private void button_gen8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_gen8ActionPerformed
        setExtendedState(1);
    }//GEN-LAST:event_button_gen8ActionPerformed

    public ArrayList<Persona> getLista() {
        return Lista;
    }

    public void setLista(ArrayList<Persona> Lista) {
        this.Lista = Lista;
    }

    public JPanel getPnl_Principal() {
        return pnl_Principal;
    }

    public void setPnl_Principal(JPanel pnl_Principal) {
        this.pnl_Principal = pnl_Principal;
    }

    

    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrame_Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Componentes.Button_gen btn_Agregar_persona;
    private Componentes.Button_gen btn_Agregar_usuario;
    private Componentes.Button_gen btn_Inicio;
    private Componentes.Button_gen btn_Mas_acciones;
    private Componentes.Button_gen btn_Mas_acciones1;
    private Componentes.Button_gen button_gen5;
    private Componentes.Button_gen button_gen6;
    private Componentes.Button_gen button_gen7;
    private Componentes.Button_gen button_gen8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel pnl_Nav;
    private javax.swing.JPanel pnl_Nav1;
    private javax.swing.JPanel pnl_Nav2;
    private javax.swing.JPanel pnl_Nav3;
    private javax.swing.JPanel pnl_Nav4;
    private javax.swing.JPanel pnl_Principal;
    // End of variables declaration//GEN-END:variables
}
